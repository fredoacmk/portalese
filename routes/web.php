<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$locale = App::getLocale();

if (App::isLocale('de')) {
    //
}

// Guest routes
Route::get('/', 'GuestController@index');
Route::get('/kontakt', 'GuestController@contact');
Route::get('/über-uns', 'GuestController@about');



Auth::routes();

Route::get('/logout', 'Auth\LoginController@userLogout')->name('user.logout'); //move
Route::post('/login', 'Auth\LoginController@login')->name('user.login.submit'); //move


Route::prefix('lesen')->group(function() {

    Route::get('/', 'UserDashboardController@index'); //move

    // User profile routes
    Route::get('/profil', 'UserSettingController@index');
    Route::post('/profil', 'UserSettingController@store'); //move

    // User Lernstatistik routes
    Route::get('/lernstatistik', 'UserStatisticController@index');

    // User Seiten routes
    Route::get('/{lebensbereich}', 'UserSeitenController@index');

    Route::get('/{lebensbereich}/{seite}', 'UserSeitenController@read');
    Route::get('/{lebensbereich}/{seite}/aufgaben', 'UserSeitenController@solve'); //move

    Route::post('/auswertung', 'ResultController@evaluate'); //move
});


Route::prefix('admin')->group(function() {

    // Admin start page route
    Route::get('/', 'AdminDashBoardController@index')->name('admin.dashboard');

    // Admin login routes
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login'); //move
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit'); //move
    Route::post('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout'); //move

    // Admin Password reset routes
    Route::post('/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email'); //move
    Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request'); //move
    Route::post('/password/reset', 'Auth\AdminResetPasswordController@reset'); //move
    Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset'); //move

    // Admin Seiten routes
    Route::get('/seiten', 'AdminSeitenController@index')->name('admin.seiten');
    Route::get('/seiten/erstellen', 'AdminSeitenController@create')->name('admin.seiten.erstellen'); //move: check

    Route::post('/seiten', 'AdminSeitenController@store'); //move: check
    Route::get('/seiten/{seite}/bearbeiten', 'AdminSeitenController@edit'); //move

    Route::delete('/seiten/{seite}/delete', 'AdminSeitenController@destroy'); //move
    Route::get('/seiten/{seite}', 'AdminSeitenController@show'); //move
    Route::post('/seiten/{seite}/bearbeiten', 'AdminSeitenController@update'); //move

    // Media routes
    Route::get('/mediathek', 'AdminMediaController@index');

    // Admin Lebensbereich routes
    Route::get('/lebensbereiche', 'AdminLebensbereichController@index')->name('admin.lebensbereiche');
    Route::post('/lebensbereiche', 'AdminLebensbereichController@store'); //move
    Route::delete('/lebensbereiche/{lebensbereich}/delete', 'AdminLebensbereichController@destroy'); //move
    Route::get('/lebensbereiche/{lebensbereich}/bearbeiten', 'AdminLebensbereichController@edit'); //move
    Route::post('/lebensbereiche/{lebensbereich}/bearbeiten', 'AdminLebensbereichController@update'); //move
    Route::get('/lebensbereiche/erstellen', 'AdminLebensbereichController@create')->name('admin.lebensbereiche.erstellen'); //move: check

    // Admin Kompetenzstufe routes
    Route::get('/kompetenzstufe', 'AdminKompetenzstufeController@index') -> name ('admin.kompetenzstufe');
    Route::post('/kompetenzstufe', 'AdminKompetenzstufeController@store')->name('admin.kompetenzstufen'); //move
    Route::get('/kompetenzstufe/{kompetenzstufe}/bearbeiten', 'AdminKompetenzstufeController@edit'); //move

    Route::delete('/kompetenzstufe/{kompetenzstufe}/delete', 'AdminKompetenzstufeController@destroy'); //move
    Route::get('/kompetenzstufe/erstellen', 'AdminKompetenzstufeController@create')->name('admin.kompetenzstufe.erstellen');
    Route::get('/kompetenzstufe/{kompetenzstufe}', 'AdminKompetenzstufeController@show');
    Route::post('/kompetenzstufe/{kompetenzstufe}', 'AdminKompetenzstufeController@update'); //move

    // Admin Snippet routes
    Route::get('/snippets', 'AdminSnippetController@index')->name('admin.snippets');
    Route::get('/snippets/erstellen', 'AdminSnippetController@create')->name('admin.snippets.erstellen'); //move
    Route::post('/snippets', 'AdminSnippetController@store'); //move
    Route::get('/snippets/{snippet}/bearbeiten', 'AdminSnippetController@edit'); //move

    Route::delete('/snippets/{snippetTypeId}/{snippetId}/delete', 'AdminSnippetController@destroy'); //move
    Route::get('/snippets/{snippet}', 'AdminSnippetController@show');
    Route::patch('/snippets/{snippet}', 'AdminSnippetController@update'); //move

    // Admin Benutzerverwaltung routes
    Route::get('/benutzerverwaltung', 'AdminBenutzerverwaltungController@index');
    Route::get('/benutzerverwaltung/erstellen', 'AdminBenutzerverwaltungController@create'); //move
    Route::post('/benutzerverwaltung', 'AdminBenutzerverwaltungController@store'); //move
    Route::get('/benutzerverwaltung/{benutzer}/bearbeiten', 'AdminBenutzerverwaltungController@edit'); //move

    Route::delete('/benutzerverwaltung/{benutzer}', 'AdminBenutzerverwaltungController@destroy'); //move
    Route::get('/benutzerverwaltung/{benutzer}', 'AdminBenutzerverwaltungController@show');
    Route::patch('/benutzerverwaltung/{benutzer}', 'AdminBenutzerverwaltungController@update'); //move

    // Admin profile routes
    Route::get('/profil', 'AdminSettingController@index');
    Route::patch('/profil', 'AdminSettingController@update');

    // Admin Lernstatistik routes
    Route::get('/lernstatistik', 'AdminStatisticController@index');
});