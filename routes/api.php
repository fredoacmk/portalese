<?php

use Illuminate\Http\Request;
use App\Seite;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Test multi middleware route
Route::prefix('v1')->group(function () {
    // API Admin routes
    Route::get('/admindashboard', 'api\ApiAdminController@getDashboardCounts');
    Route::get('/pagesoverview', 'api\ApiAdminController@getPagesOverview');
    Route::get('/snippetsoverview', 'api\ApiAdminController@getSnippetsOverview');
    Route::get('/areaofinterestoverview', 'api\ApiAdminController@getAreaOfInterestOverview');
    Route::get('/adminskilllevel', 'api\ApiAdminController@getSkillLevels');

    // API User routes
    Route::get('/userdashboard', 'api\ApiUserController@getDashboard');
    Route::get('/snippets/{snippet}', 'api\ApiUserController@getSnippets');

    // API Forms routes
    Route::get('/interest', 'api\ApiFormsController@getInterestList');
    Route::get('/competence', 'api\ApiFormsController@getCompetenceList');
    Route::get('/getsnippetdropdown', 'api\ApiFormsController@getSnippetDropdown');
});