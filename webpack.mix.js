let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/leseportal-admin.js', 'public/js/')
    .js('resources/assets/js/leseportal-user.js', 'public/js')
    .js('resources/assets/js/leseportal-home.js', 'public/js')
   .sass('resources/assets/sass/leseportal-admin.scss', 'public/css')
   .sass('resources/assets/sass/leseportal-user.scss', 'public/css')
   .sass('resources/assets/sass/leseportal-stylesheet.scss', 'public/css')
   .copyDirectory('resources/assets/fonts/Asap', './public/fonts/Asap');
