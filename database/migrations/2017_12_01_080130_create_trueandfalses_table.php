<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrueandfalsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trueandfalses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idSeite')->unsigned();
            $table->foreign('idSeite')
                ->references('idSeite')->on('seiten')
                ->onDelete('cascade');
            $table->integer('snippetTypeId')->default(2);
            $table->string('statement');
            $table->boolean('richtig');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trueandfalses');
    }
}
