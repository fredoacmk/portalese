<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDraganddropsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('draganddrops', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idSeite')->unsigned();
            $table->foreign('idSeite')
                ->references('idSeite')->on('seiten')
                ->onDelete('cascade');
            $table->integer('snippetTypeId')->default(3);
            $table->string('beschreibung');

            /*
            $table->integer('idSnippet')->unsigned();
            $table->foreign('idSnippet')
                ->references('idSnippet')->on('snippets')
                ->onDelete('cascade');
            */
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('draganddrops');
    }
}
