<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeitetosnippetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seitetosnippets', function (Blueprint $table) {
            $table->integer('seite_idSeite')->unsigned();
            $table->integer('snippet_idSnippet')->unsigned();

            $table->foreign('seite_idSeite')
                ->references('idSeite')->on('seiten')
                ->onDelete('cascade');
            $table->foreign('snippet_idSnippet')
                ->references('idSnippet')->on('snippets')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seitetosnippets');
    }
}
