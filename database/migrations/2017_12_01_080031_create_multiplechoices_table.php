<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMultiplechoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('multiplechoices', function (Blueprint $table) {
            $table->increments('idMultipleChoice');
            $table->string('beschreibung');

            $table->integer('idSnippet')->unsigned();
            $table->foreign('idSnippet')
                ->references('idSnippet')->on('snippets')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('multiplechoices');
    }
}
