<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeitenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seiten', function (Blueprint $table) {

            $table->increments('idSeite');

            $table->string('titel');

            $table->text('inhalt');

            $table->integer('idLebensbereich')->unsigned();
            $table->integer('idKompetenzstufe')->unsigned();

            $table->foreign('idLebensbereich')->references('idLebensbereich')->on('lebensbereiche')->onDelete('cascade');

            $table->foreign('idKompetenzstufe')->references('idKompetenzstufe')->on('kompetenzstufen')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seiten');
    }
}
