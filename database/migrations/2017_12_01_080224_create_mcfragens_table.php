<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMcfragensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mcfragens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idSeite')->unsigned();
            $table->foreign('idSeite')
                ->references('idSeite')->on('seiten')
                ->onDelete('cascade');
            $table->integer('snippetTypeId')->default(1);
            $table->string('frage')->nullable();
            $table->string('antwort1')->nullable();
            $table->string('antwort2')->nullable();
            $table->string('antwort3')->nullable();
            $table->string('antwort4')->nullable();
            $table->string('antwort5')->nullable();
            $table->boolean('richtig1')->nullable();
            $table->boolean('richtig2')->nullable();
            $table->boolean('richtig3')->nullable();
            $table->boolean('richtig4')->nullable();
            $table->boolean('richtig5')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mcfragens');
    }
}
