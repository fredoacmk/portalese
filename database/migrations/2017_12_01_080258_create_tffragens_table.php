<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTffragensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tffragens', function (Blueprint $table) {
            $table->increments('idTFFrage');
            $table->boolean('antwort');

            $table->integer('id')->unsigned();
            $table->foreign('id')
                ->references('id')->on('trueandfalses')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tffragens');
    }
}
