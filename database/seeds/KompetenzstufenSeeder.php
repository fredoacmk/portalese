<?php

use Illuminate\Database\Seeder;

class KompetenzstufenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kompetenzstufen')->insert([
            'level' => 'A1',
            'beschreibung' => 'Sie können eine einfache Frage verstehen und beantworten.',
        ]);
        DB::table('kompetenzstufen')->insert([
            'level' => 'A2',
            'beschreibung' => 'Sie können sich in vertrauten Situationen mit kurzen Sätzen verständigen.',
        ]);
        DB::table('kompetenzstufen')->insert([
            'level' => 'B1',
            'beschreibung' => 'Sie finden sich in fast allen Alltagssituationen sprachlich zurecht.',
        ]);
        DB::table('kompetenzstufen')->insert([
            'level' => 'B2',
            'beschreibung' => 'Sie können Ihre Meinung zu vielen Themen spontan und klar ausdrücken.',
        ]);
        DB::table('kompetenzstufen')->insert([
            'level' => 'C1',
            'beschreibung' => 'Sie kommunizieren fliessend, differenziert und praktisch fehlerfrei.',
        ]);
        DB::table('kompetenzstufen')->insert([
            'level' => 'C2',
            'beschreibung' => 'Sie beherrschen die Sprache fast so gut wie Ihre Muttersprache.',
        ]);
    }
}
