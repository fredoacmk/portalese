<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminsTableSeeder::class);
        $this->call(KompetenzstufenSeeder::class);
        $this->call(LebensbereicheTableSeeder::class);
        $this->call(SeitenTableSeeder::class);
        $this->call(SnippetsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        //$this->call(SnippetsToSeitenSeeder::class);
        //$this->call(MultiplechoiceSeeder::class);
        $this->call(MultiplechoiceQuestionSeeder::class);
        $this->call(TrueAndFalsesTableSeeder::class);
        //$this->call(TFFragensTableSeeder::class);
    }
}
