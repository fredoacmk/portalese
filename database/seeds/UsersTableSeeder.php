<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds for Users.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'user',
            'email' => 'user@leseportal.dev',
            'password' => app('hash')->make('Testing12345'),
            'remember_token' => str_random(10),
        ]);

    }
}
