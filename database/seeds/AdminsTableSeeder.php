<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds for Users.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'name' => 'admin',
            'email' => 'admin@leseportal.dev',
            'role' => 'admin',
            'password' => app('hash')->make('Testing12345'),
            'remember_token' => str_random(10),
        ]);
        DB::table('admins')->insert([
            'name' => 'Zwahlen',
            'email' => 'ezwahlen@mac.com',
            'role' => 'admin',
            'password' => app('hash')->make('Leseportal123'),
            'remember_token' => str_random(10),
        ]);
        DB::table('admins')->insert([
            'name' => 'Naef',
            'email' => 'roland@naefpiera.ch',
            'role' => 'admin',
            'password' => app('hash')->make('Leseportal123'),
            'remember_token' => str_random(10),
        ]);

    }
}
