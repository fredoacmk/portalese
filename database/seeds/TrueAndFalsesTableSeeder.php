<?php

use Illuminate\Database\Seeder;

class TrueAndFalsesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('trueandfalses')->insert([
            'statement' => 'Präsident Trump trägt ein Toupet.',
            'richtig' => false,
            'idSeite' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('trueandfalses')->insert([
            'statement' => 'Präsident Macron trägt ein Toupet.',
            'richtig' => false,
            'idSeite' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('trueandfalses')->insert([
            'statement' => 'Präsident Macron versucht die strategisch politische Position von Bundeskanzelerin Merkel einzunehmen.',
            'richtig' => true,
            'idSeite' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('trueandfalses')->insert([
            'statement' => 'Beim Iran-Deal geht es Franksreichs Waffenlieferungen an den Iran.',
            'richtig' => false,
            'idSeite' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('trueandfalses')->insert([
            'statement' => 'Macron hat auch schon richtige Brücken gebaut. Eine steht in Bern.',
            'richtig' => false,
            'idSeite' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);
    }
}
