<?php

use Illuminate\Database\Seeder;

class SnippetsToSeitenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('seitetosnippets')->insert([
            'seite_idseite' => '1',
            'snippet_idsnippet' => '1',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('seitetosnippets')->insert([
            'seite_idseite' => '2',
            'snippet_idsnippet' => '2',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('seitetosnippets')->insert([
        'seite_idseite' => '1',
        'snippet_idsnippet' => '2',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('seitetosnippets')->insert([
            'seite_idseite' => '1',
            'snippet_idsnippet' => '1',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('seitetosnippets')->insert([
            'seite_idseite' => '1',
            'snippet_idsnippet' => '3',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);
    }
}
