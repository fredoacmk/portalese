<?php

use Illuminate\Database\Seeder;

class MultiplechoiceQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mcfragens')->insert([
            'frage' => 'Wie heisst der Hauptcharakter im Film?',
            'antwort1' => 'Nemo',
            'antwort2' => 'Nano',
            'antwort3' => 'Neo',
            'antwort4' => 'Borneo',
            'antwort5' => null,
            'richtig1' => false,
            'richtig2' => false,
            'richtig3' => true,
            'richtig4' => false,
            'richtig5' => null,
            'idSeite' => '2',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('mcfragens')->insert([
            'frage' => 'Welche Pille muss man nehmen, um in die Matrix eingelassen zu werden?',
            'antwort1' => 'Blau',
            'antwort2' => 'Rot',
            'antwort3' => null,
            'antwort4' => null,
            'antwort5' => null,
            'richtig1' => false,
            'richtig2' => true,
            'richtig3' => null,
            'richtig4' => null,
            'richtig5' => null,
            'idSeite' => '2',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('mcfragens')->insert([
            'frage' => 'Wie heisst der Glatzkopf mit den runden Brillengläser?',
            'antwort1' => 'Martin',
            'antwort2' => 'Morpheus',
            'antwort3' => 'Morten',
            'antwort4' => 'Morphin',
            'antwort5' => null,
            'richtig1' => false,
            'richtig2' => true,
            'richtig3' => false,
            'richtig4' => false,
            'richtig5' => null,
            'idSeite' => '2',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('mcfragens')->insert([
            'frage' => 'Schneckenzecken?',
            'antwort1' => 'Nemo',
            'antwort2' => 'Nano',
            'antwort3' => 'Neo',
            'antwort4' => 'Borneo',
            'antwort5' => null,
            'richtig1' => false,
            'richtig2' => false,
            'richtig3' => true,
            'richtig4' => false,
            'richtig5' => null,
            'idSeite' => '2',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('mcfragens')->insert([
            'frage' => 'Welche Aussagen über den Iran Deal sind korrekt?',
            'antwort1' => 'Präsident Macron wird Präsident Trump (laut einem Korrespontenden aus Brüssel) davon abbringen können, aus dem Iran-Deal auszusteigen.',
            'antwort2' => 'Der Iran-Deal regelt die Einreise von iranischen Bürgern in die USA & Frankreich.',
            'antwort3' => 'Der Iran-Deal wurde bereits im 2017 durch Präsident Obama anulliert.',
            'antwort4' => 'Der Iran-Deal regelt ob und zu welchem Zweck der Iran Uran anreichern werden darf.',
            'antwort5' => null,
            'richtig1' => true,
            'richtig2' => false,
            'richtig3' => false,
            'richtig4' => true,
            'richtig5' => null,
            'idSeite' => '1',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('mcfragens')->insert([
            'frage' => 'Welche Aussagen über Präsident Macron sind korrekt?',
            'antwort1' => 'Präsident Macron war der vorherige Präsident Frankreichs.',
            'antwort2' => 'Präsident Macron ist der aktuelle Präsident Frankreichs.',
            'antwort3' => 'Präsident Macron besuchte den amerikanischen Präsidenten im weissen Haus',
            'antwort4' => 'Präsident Macron sowie Präsident Trump kritisieren den Handelsüberschuss der USA.',
            'antwort5' => null,
            'richtig1' => false,
            'richtig2' => true,
            'richtig3' => true,
            'richtig4' => true,
            'richtig5' => null,
            'idSeite' => '1',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
