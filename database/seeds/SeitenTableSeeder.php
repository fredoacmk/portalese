<?php

use Illuminate\Database\Seeder;

class SeitenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $text2 = <<<'EOT'
<h2>Europas Früchte- und Gemüsegarten lechzt nach Wasser</h2>
<ul>
	<li>In Murcia unterh&auml;lt Spanien eine Hochleistungs-Landwirtschaft. M&ouml;glich auch dank Wasser aus dem Tajo.</li>
	<li>Der Fluss f&uuml;hrt immer weniger Wasser. Die D&uuml;rre-Jahre setzten den Ernten in der s&uuml;dostspanischen Region zu.</li>
	<li>Meerentsalzungsanlagen sind bisher erst ein Teil der L&ouml;sung. Deren Kapazit&auml;t m&uuml;sste gesteigert werden.</li>
</ul>
<p><img alt="Spaniens «Früchte- und Gemüsegarten» in der Region Murcia." src="https://www.srf.ch/var/storage/images/_aliases/640w/auftritte/news/bilder/2018/04/17/node_15504087/172700177-2-ger-DE/bild.jpg" style="height:225px; width:400px" title="Spaniens «Früchte- und Gemüsegarten» in der Region Murcia." /></p>
<p>Legende:Trotz innovativer Bew&auml;sserungstechnik hat Murcia Ernteeinbr&uuml;che hinnehmen m&uuml;ssen.SRF</p>
<p>Murcia schm&uuml;ckt sich heute mit dem &Uuml;bernamen &laquo;Fr&uuml;chte- und Gem&uuml;segarten Europas&raquo;. M&ouml;glich wurde dieser Aufstieg dank eines 300 Kilometer langen Kanals. Er f&uuml;hrt seit den fr&uuml;hen 1980er Jahren Wasser vom Oberlauf des Tajo in den S&uuml;den. Damit dort Felder und Plantagen bew&auml;ssert werden k&ouml;nnen. In Murcia hat das zu einem eigentlichen Boom gef&uuml;hrt, obschon vom Tajo deutlich weniger Wasser kam, als urspr&uuml;nglich versprochen worden war. Die Experten hatten sich versch&auml;tzt.</p>
<p>Murcia wuchs dennoch, wurde produktiver und setzte sich in der Gem&uuml;se- und Fr&uuml;chteproduktion landesweit an die Spitze. Tomaten, Peperoni, Auberginen, Salate oder Zitronen, Grapefruit, Orangen, Aprikosen: alles kommt aus der Hochleistungslandwirtschaft von Murcia.</p>
<p><img alt="Zitronenbaum-Hain mit jungen, eingepackten Bäumchen und Tröpfchenbewässerungsrohren." src="https://www.srf.ch/var/storage/images/_aliases/640w/auftritte/news/bilder/2018/04/18/node_15511337/172765812-2-ger-DE/bild.jpg" style="height:225px; width:400px" title="Zitronenbaum-Hain mit jungen, eingepackten Bäumchen und Tröpfchenbewässerungsrohren." /></p>
<p>Legende:Mit der Tr&ouml;pfchenbew&auml;sserung durch Rohre auf dem Boden, kann ein Drittel Wasser gespart werden.SRF</p>
<p>Je nach Produkt wird mehr als nur einmal pro Jahr geerntet. Die gr&ouml;ssten M&auml;rkte sind Deutschland, Frankreich, Grossbritannien und Italien. Aber Murcia exportiert viel mehr. Insgesamt hat die Region Kunden in 88 L&auml;ndern.</p>
<p><img alt="Die Region Murcia in Südostspanien. Eingepackte Reben so weit das Auge reicht." src="https://www.srf.ch/var/storage/images/_aliases/640w/auftritte/news/bilder/2018/04/17/node_15504347/172702777-2-ger-DE/bild.jpg" style="height:225px; width:400px" title="Die Region Murcia in Südostspanien. Eingepackte Reben so weit das Auge reicht." /></p>
<p>Legende:Die Region Murcia in S&uuml;dostspanien ist ein Fr&uuml;chtegarten f&uuml;r halb Europa. Noch.SRF</p>
<p>Die D&uuml;rre-Jahre aber waren schwierig f&uuml;r Murcia und bremsten die Produktion. Vom Tajo kam noch weniger Wasser. In Murcia haben darum praktisch alle Bauern Tr&ouml;pfchen-Bew&auml;sserung eingerichtet. Das Wasser wird in einer Rohrleitung direkt zu den Pflanzen gef&uuml;hrt und dort tr&ouml;pfchenweise abgegeben. So werden Einsparungen von bis zu 30 Prozent m&ouml;glich.</p>
<h2>Von mediterran auf subtropisch</h2>
<p>Keine andere Region Spaniens hat so konsequent umgestellt auf sparsameren Wassergebrauch. Dennoch gab es Ernteverluste wegen des Wassermangels. Und man arbeitet bereits daran, die Produktion auf subtropische Fr&uuml;chte umzustellen: Der Klimawandel l&auml;sst kaum eine andere Wahl.</p>
<p><img alt="Spaniens «Früchte- und Gemüsegarten» in der Region Murcia. Ein Zitronenhain." src="https://www.srf.ch/var/storage/images/_aliases/640w/auftritte/news/bilder/2018/04/17/node_15504082/172700142-4-ger-DE/bild.jpg" style="height:225px; width:400px" title="Spaniens «Früchte- und Gemüsegarten» in der Region Murcia. Ein Zitronenhain." /></p>
<p>Legende:Bereits wird mit subtropischen Fr&uuml;chten experimentiert, die weniger Wasser brauchen als diese Orangen.SRF</p>
<p>Trotz aller Anstrengungen hat Murcia weitere schwierige Jahre vor sich. Kaum jemand rechnet mit einer Umkehr des Trends zur Erw&auml;rmung. Das Wasser d&uuml;rfte weiter knapp bleiben.</p>
<p>8.Mrz.15.Mrz.22.Mrz.29.Mrz.5.Apr.12.Apr.19.Apr.26.Apr.3.Mai10.Mai17.Mai24.Mai31.MaiNiederschlagsmengemm0mm20mm40mm60mm80mm100</p>
<p>Mittel der Kumulierten Niederschl&auml;ge im Zeitraum 1981-2010Kumulierte Niederschl&auml;ge M&auml;rz bis Mai 2018</p>
<p>Legende:</p>
<p>Niederschl&auml;ge in MurciaKumulierte Niederschl&auml;ge in der Region Murcia M&auml;rz bis Mai 2018LA AGENCIA ESTATAL DE METEOROLOG&Iacute;A (AEMET)</p>
<p>Zwar will noch niemand davon reden in Murcia, aber der Kanal, der &uuml;ber drei Jahrzehnte lang Wasser vom Tajo brachte, k&ouml;nnte in den kommenden Jahren immer h&auml;ufiger trocken bleiben. Und auch die L&ouml;sung &uuml;ber Meerwasser ist nicht so einfach, wie man sich das allenfalls vorstellen m&ouml;chte.</p>
<p><img alt="Entsalzungsanlage." src="https://www.srf.ch/var/storage/images/_aliases/640w/auftritte/news/bilder/2018/04/18/node_15509987/172754292-2-ger-DE/bild.jpg" style="height:225px; width:400px" title="Entsalzungsanlage." /></p>
<p>Legende:Ob das Wasser aus Entsalzungsanlagen gut f&uuml;r Pflanzen ist, dar&uuml;ber scheiden sich die Geister.SRF</p>
<p>Spanien hat zwar die gr&ouml;sste Meerwasserentsalzungsanlage Europas. Sie steht in Torrevieja, etwa 70 Kilometer von der Stadt Murcia entfernt. Der Bau blieb aber wegen politischer Querelen trotz Trockenheit jahrelang blockiert.</p>
<p>Jetzt funktioniert die Anlage erst auf halber Kraft, kann also erst die H&auml;lfte des Wassers liefern, die m&ouml;glich w&auml;re. Grund: Die Stromleitung, die volle Kraft liefert, wird erst diesen Sommer fertig, drei Jahre nach der Er&ouml;ffnung der Anlage.</p>

EOT;

        $Macron = <<<'EOT'
        <h2>«Macron könnte der Brückenbauer sein»</h2>
<p><img alt="Macron und Trump von hinten, durch einen prunkvollen Flur im Weissen Haus schreitend. Macron umarmt Trump." src="https://www.srf.ch/var/storage/images/_aliases/640w/auftritte/news/bilder/2018/04/25/node_15561087/173213977-2-ger-DE/bild.jpg" style="height:281px; width:500px" title="Macron und Trump von hinten, durch einen prunkvollen Flur im Weissen Haus schreitend. Macron umarmt Trump." /></p>
<p>Legende:Wie allerbeste Freunde: Ein bisschen Show zwar, doch vielleicht auch der Beginn eines Tauwetters zwischen der EU und den USA.REUTERS</p>
<p>Zur Begr&uuml;ssung gab es K&uuml;sschen, und im Garten des Weissen Hauses pflanzten die beiden einen Baum. Frankreichs Pr&auml;sident Emmanuel Macron und US-Pr&auml;sident Donald Trump haben sich der &Ouml;ffentlichkeit als ziemlich gute Freunde pr&auml;sentiert.</p>
<p>Auch im Streit um das Atomabkommen mit Iran k&ouml;nnte es zu einer Ann&auml;herung kommen. Ein Teilerfolg f&uuml;r Macron, wie Korrespondent Sebastian Ramspeck in Br&uuml;ssel bilanziert. Doch er bezweifelt, dass es Macron gelingen k&ouml;nnte, Trump von einem R&uuml;ckzug aus dem Iran-Deal abzuhalten.</p>
<p><strong>SRF News: K&ouml;nnte Macron zu einem Br&uuml;ckenbauer zwischen den USA und der EU werden?</strong><br />
Sebastian Ramspeck: Wenn es einem gelingen k&ouml;nnte, eine Br&uuml;cke zwischen der EU und den USA zu bauen, dann ganz sicher Emmanuel Macron. Er hatte Trump mit der Milit&auml;rparade am franz&ouml;sischen Nationalfeiertag bereits im vergangenen Jahr schwer beeindruckt &ndash; der US-Pr&auml;sident war dort Ehrengast. Gestern wirkten die beiden wie allerengste Freunde: Sie hielten H&auml;ndchen, umarmten und k&uuml;ssten sich. Das war nat&uuml;rlich auch etwas Show und Kalk&uuml;l.</p>
<blockquote>
<p>Wenn es einem gelingen k&ouml;nnte, eine Br&uuml;cke zwischen der EU und den USA zu bauen, dann ganz sicher Emmanuel Macron.</p>
</blockquote>
<p>Macron ist aber sicher der europ&auml;ische Politiker, zu dem Trump das engste Verh&auml;ltnis aufgebaut hat. Allerdings hat der franz&ouml;sische Pr&auml;sident diese Herzlichkeit bisher noch nicht in einen konkreten Verhandlungserfolg umsetzen k&ouml;nnen.</p>
<p><strong>Wie kommt das gute Verh&auml;ltnis zwischen Macron und Trump in der EU an, gerade im Hinblick auf das Atomabkommen mit Iran?</strong></p>
<p>Die EU ist wirtschaftlich zwar ein Schwergewicht, politisch kann sie aber oft nicht sehr viel in die Waagschale werfen. Einer wie Macron ist dort hoch willkommen, denn die EU kann ihre Interessen auf internationaler Ebene nur mit starken Pers&ouml;nlichkeiten vertreten.</p>
<blockquote>
<p>Nur wenn Macron auch einen konkreten Verhandlungserfolg vorweisen kann, ist das auch wirklich ein Erfolg f&uuml;r ihn.</p>
</blockquote>
<p>Vom fr&uuml;heren US-Aussenminister Henry Kissinger stammt der ber&uuml;hmte Satz: &laquo;Wen rufe ich an, wenn ich Europa anrufen will.&raquo; Trump hat jetzt eine Telefonnummer, n&auml;mlich die von Macron. Und das st&auml;rkt nat&uuml;rlich auch Macrons Position als F&uuml;hrungsfigur innerhalb der EU. Aber nur, wenn er dann auch irgendwann einmal einen konkreten Verhandlungserfolg vorweisen kann, ist das auch wirklich ein Erfolg f&uuml;r ihn.</p>
<p><a href="https://www.srf.ch/news/international/macron-und-trump-zu-iran-hat-macron-erreicht-was-er-wollte"><img alt="Hat Macron erreicht, was er wollte?" src="https://www.srf.ch/var/storage/images/_aliases/288w/auftritte/news/bilder/2018/04/24/c418cee92d560c4346c766eb0df1e080/173203762-2-ger-DE/c418cee92d560c4346c766eb0df1e080.jpg" /></a></p>
<p><strong>In der EU gab bisher die Wirtschaftsmacht Deutschland den Ton an. Bedroht die neue franz&ouml;sisch-amerikanische Freundschaft Bundeskanzlerin Angela Merkel?</strong></p>
<p>Ich stelle mir vor, dass Merkel ihren neuen franz&ouml;sischen Partner sehr zwiesp&auml;ltig sieht. Sie ist eine Pragmatikerin. Als solche hat sie Interesse an einem starken franz&ouml;sischen Pr&auml;sidenten, der die gemeinsamen Interessen der EU-Staaten zum Beispiel gegen&uuml;ber den USA durchsetzen kann. Gleichzeitig ist auch klar, dass Macron den Platz einnehmen will, den Merkel jahrelang innehatte.</p>
<p>Er will der Primus der EU-Staatschefs sein und f&uuml;r die Union den Takt und die Richtung vorgeben. Damit kann er Deutschland sehr unangenehm werden, denn in einem Punkt steht Macron dem amerikanischen Pr&auml;sidenten n&auml;her, als Merkel lieb sein kann: Beide kritisieren den gigantischen Handels&uuml;berschuss Deutschlands und fordern Korrekturen.</p>
<blockquote>
<p>Es ist klar, dass Macron den Platz einnehmen will, den Merkel jahrelang innehatte.</p>
</blockquote>
<p><a href="https://www.srf.ch/news/international/staatsbesuch-in-den-usa-macron-will-trump-von-dauerhaftem-syrien-engagement-ueberzeugen"><img alt="Macron will Trump von dauerhaftem Syrien-Engagement überzeugen" src="https://www.srf.ch/var/storage/images/_aliases/288w/auftritte/news/bilder/2018/04/23/e7eb54c7d941f0464951e485f9ac9e0e/173116297-2-ger-DE/e7eb54c7d941f0464951e485f9ac9e0e.jpg" /></a></p>
<p><strong>Macron hat Trump gegen&uuml;ber signalisiert, auch er sei an einem neuen Atomabkommen mit Iran interessiert. Schafft er es, daf&uuml;r seine europ&auml;ischen Partner an Bord holen?</strong></p>
<p>Macron weicht mit seinem Vorschlag eines neuen, viel umfassenderen Iran-Deals von der offiziellen EU-Position ab. F&uuml;r Br&uuml;ssel war die Atomfrage immer das Eine. Den Einfluss Irans beispielsweise in Syrien will die EU als separates Problem angehen. Macron aber will beide Themen in einem umfassenden Iran-Deal miteinander verkn&uuml;pfen, in dem das bisherige Atomabkommen als einer von mehreren Pfeilern weiter existieren w&uuml;rde.</p>
<blockquote>
<p>Im Moment spricht vieles daf&uuml;r, dass sich die USA Mitte Mai wie angedroht aus dem Atomabkommen mit Iran zur&uuml;ckziehen werden.</p>
</blockquote>
<p>Wenn Macron Trump auf diese Weise tats&auml;chlich davon &uuml;berzeugen k&ouml;nnte, das Atomabkommen beizubehalten, w&uuml;rde die EU das wohl oder &uuml;bel schlucken m&uuml;ssen. Das w&auml;re besser als gar nichts. Und das w&auml;re dann ein erster grosser Erfolg f&uuml;r Macron. Im Moment aber spricht vieles daf&uuml;r, dass sich die USA Mitte Mai wie angedroht aus dem Atomabkommen mit Iran zur&uuml;ckziehen werden und es Macron nicht gelingt, im Iran-Dossier den grossen Durchbruch zu erzielen.</p>
EOT;
        $velo = <<<'EOT'
        <h1>Neue Messungen-Das Velo wird in der Stadt Bern immer beliebter</h1>
<p>Von 2014 bis 2017 hat der Veloverkehr in der Stadt Bern um 35 Prozent zugenommen. Noch bleibt aber viel Arbeit.</p>
<p><img alt="Zwei Velofahrende auf der Strasse" src="https://www.srf.ch/var/storage/images/_aliases/640w/auftritte/news/bilder/2018/04/23/node_15549397/173104947-2-ger-DE/bild.jpg" style="height:281px; width:500px" title="Zwei Velofahrende auf der Strasse" /></p>
<p>Wie viele Autos, Motorr&auml;der und andere motorisierte Fahrzeuge durch Bern fahren, misst die Stadt schon l&auml;nger. Neu gibt es auch Zahlen zum Veloverkehr. Seit 2014 misst die Stadt Bern an verschiedenen Standorten vorbeifahrende Velofahrerinnen und Velofahrer. Nun hat sie die Messwerte erstmals vorgestellt.</p>
<p>&nbsp;</p>
<h2>Zunahme um 35 Prozent</h2>
<p>Die Zahlen sind unterschiedlich: Auf der Monbijoustrasse wurden 2017 pro Tag im Schnitt 4068 Velos gez&auml;hlt, das sind rund 1000 mehr als im Jahr 2014. Auf der Weissensteinstrasse, die 2014 im t&auml;glichen Schnitt 264 Velofahrende z&auml;hlte, waren es vier Jahre sp&auml;ter lediglich 26 mehr. Ein bescheidenes Wachstum. Insgesamt aber zeigen alle Messwerte nach oben. Werden die Werte im Jahr 2014 mit 2017 verglichen, wird klar: Der Veloverkehr nahm insgesamt um 35 Prozent zu.</p>
<blockquote>
<p>Das freut uns immens.</p>
</blockquote>
<p>&laquo;Das ist ein sehr grosser Wert und freut uns immens&raquo;, sagt der Stadtberner Verkehrsplaner Karl Vogel. &laquo;Diese Zahlen zeigen uns, dass wir das Velonetz so rasch wie m&ouml;glich ausbauen m&uuml;ssen.&raquo; Das heisse breite Velostreifen, gen&uuml;gend Veloabstellpl&auml;tze, gute L&ouml;sungen bei Bushaltestellen und Kreuzungen.</p>
<blockquote>
<p>Jetzt wird es schwieriger.</p>
</blockquote>
<p>Bis ins Jahr 2020 soll der Anteil an Velofahrenden am Gesamtverkehr in der Stadt Bern bei 20 Prozent liegen. Bei der letzten Messung 2015 lag er bei 15 Prozent. &laquo;Die Zahlen zum Veloverkehr zeigen, dass wir gut unterwegs sind&raquo;, sagt Verkehrsdirektorin Ursula Wyss. Jetzt werde es aber m&ouml;glicherweise schwieriger, denn es gehe nun auch um Strassen, an denen es nicht so einfach sei, Platz f&uuml;r Velos zu finden. &laquo;Umso wichtiger ist es zu zeigen, wie viele Leute es betrifft.&raquo;</p>
<p>&nbsp;</p>
<h2>Velobarometer zeigen Tagesstand</h2>
<p>Zeigen will das die Stadt unter anderem mit drei Veloz&auml;hlern, die f&uuml;r alle sichtbar sind: auf dem Bubenbergplatz, im Monbijou und an der Lorrainebr&uuml;cke. Die sogenannten Velobarometer zeigen den jeweiligen Tages- und den Jahresstand an Velofahrenden am betreffenden Ort an.</p>
EOT;


        DB::table('seiten')->insert([
            'titel' => '«Macron könnte der Brückenbauer sein»',
            'inhalt' => $Macron,
            'idLebensbereich' => 9,
            'idKompetenzstufe' => 2,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('seiten')->insert([
            'titel' => 'Europas Früchte- und Gemüsegarten lechzt nach Wasser',
            'inhalt' => $text2,
            'idLebensbereich' => 1,
            'idKompetenzstufe' => 4,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('seiten')->insert([
            'titel' => 'Das Velo wird in der Stadt Bern immer beliebter',
            'inhalt' => $velo,
            'idLebensbereich' => 4,
            'idKompetenzstufe' => 4,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);
    }
}
