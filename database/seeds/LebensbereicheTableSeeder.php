<?php

use Illuminate\Database\Seeder;

class LebensbereicheTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lebensbereiche')->insert([
            'name' => 'Ernährung & Essen',
            'beschreibung' => 'Alles rund um das Thema Essen und wie man sich gesund ernähren kann.',
        ]);
        DB::table('lebensbereiche')->insert([
            'name' => 'Kunst',
            'beschreibung' => 'Andere Kunstformen nebst Musik & Film.',
        ]);
        DB::table('lebensbereiche')->insert([
            'name' => 'Reisen & Ferien',
            'beschreibung' => 'Reiseberichte und Co.',
        ]);
        DB::table('lebensbereiche')->insert([
            'name' => 'Sport',
            'beschreibung' => 'Texte über Fussball, Volleyball und Co.',
        ]);
        DB::table('lebensbereiche')->insert([
            'name' => 'Tierwelt',
            'beschreibung' => 'Alles über Säugetiere, Insekten und Fische und andere lustige Erdenbewohner.',
        ]);
        DB::table('lebensbereiche')->insert([
            'name' => 'Film',
            'beschreibung' => 'Alles über Filme und Serien.',
        ]);
        DB::table('lebensbereiche')->insert([
            'name' => 'Humor',
            'beschreibung' => 'Lustige Texte',
        ]);
        DB::table('lebensbereiche')->insert([
            'name' => 'Umwelt',
            'beschreibung' => 'Texte über Umweltthemen wie Abfall, Energie, Biodiversität usw.',

        ]);
        DB::table('lebensbereiche')->insert([
            'name' => 'Politik',
            'beschreibung' => 'Eher langweilige Texte über Politik.',
        ]);
        DB::table('lebensbereiche')->insert([
            'name' => 'Wirtschaft',
            'beschreibung' => 'Apple, Samsung, Migros und Co.',
        ]);
        DB::table('lebensbereiche')->insert([
            'name' => 'Wissen & Wissenschaft',
            'beschreibung' => 'Spannende Texte aus der Welt der Wissenschaft.',
        ]);
        DB::table('lebensbereiche')->insert([
            'name' => 'Medizin & Gesundheit',
            'beschreibung' => 'Bleib gesund durch lesen dieser Texte.',
        ]);
        DB::table('lebensbereiche')->insert([
            'name' => 'Technik & Smartphones',
            'beschreibung' => 'Spannende Texte aus der Welt von Smartphones, Virtual Reality, Bitcoin und Co.',
        ]);
        DB::table('lebensbereiche')->insert([
            'name' => 'Musik',
            'beschreibung' => 'NzNzNzNz.',
        ]);
        DB::table('lebensbereiche')->insert([
            'name' => 'Games',
            'beschreibung' => 'PC-, Smartphone- und Konsolengames',
        ]);
        DB::table('lebensbereiche')->insert([
            'name' => 'Social Media & Promis',
            'beschreibung' => 'Texte über Promis, Instagram, Facebook, Twitter und Co.',
        ]);
        DB::table('lebensbereiche')->insert([
            'name' => 'Psychologie & Persönlichkeit',
            'beschreibung' => 'Spannende Texte aus der Psyschologie & Persönblichkeitsforschung',
        ]);
        DB::table('lebensbereiche')->insert([
            'name' => 'Apps',
            'beschreibung' => 'Texte über die neusten und spannendsten Apps aus AppStore und Google PlayStore',
        ]);
    }

}
