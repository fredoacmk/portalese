<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Seite::class, function (Faker $faker) {

    return [
        'titel' => $faker->sentence,
        'inhalt' => $faker->randomHtml(),
        'idLebensbereich' => $faker->numberBetween(0,12),
        'idKompetenzstufe' => $faker->numberBetween(0,7),
        'created_at' => $faker->dateTimeBetween('-2 years', '-3 days'),
        'updated_at' => $faker->dateTimeBetween('-2 days', 'now')
    ];
});
