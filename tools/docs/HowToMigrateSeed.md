# How to migrate and seed

**Declarations:**

**Migration:**

In folder database/migrations there are schemas for the tables. These are schemas to create a table. What attributes a table has, what datatype, what constraints etc.

When running a migration, with these schemas the tables are created accordingly in MySQL or equal server.

**Seeds:**

Seeds are example data going into the tables after migrating the tables to MySQL. Seeds are located in database/seeds.

## Getting Started

These instruction will get you started with migrations and seeds. After this instruction you should have the tables in MySQL and example data.

##Important

You only need to migrate once. In development you can always undo and delete tables, no worries!

### Prerequisites

What things you first need to start:

```
MySQL up, running and configured 
-> (password and user)
 
.env configured to access MySQL
``` 

## What to do?

### Creating the "leseportal" database

If your database is fresh and empty do the following, otherwise skip this step.

Go to your phpmyadmin page and click the *new* icon (on the left above the other databases) to create a new database. call it "leseportal" and set the encoding to utf8mb4-unicode-ci. click create.

### Migrating the tables
Create tables according to schema:

**`php artisan migrate`**
 
Roll back all of your application's migrations (or delete manually):
 
**`php artisan migrate:reset`**

Usually you only need these two commands, but there are some more.

> Rollback the latest migration operation

>**`php artisan migrate:rollback`**

>Rollback a limited number of migrations:

>**`php artisan migrate:rollback --step=5`**


>The migrate:refresh command will roll back all of your migrations and then execute the migrate command.

>**`php artisan migrate:refresh`**

>Refresh the database and run all database seeds...

>**`php artisan migrate:refresh --seed`**


### Seeding data

Seeding the data to DB (this calls DatabaseSeeder.php where you can call other seeds):

**`php artisan db:seed`**

Seeding specific file:

**`php artisan db:seed --class=UsersTableSeeder`**


## Troubelshooting

Google