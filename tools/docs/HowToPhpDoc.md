# How to setup and use PhpDocumentor

**Introduction:**

PhpDocumentor is a tool that makes it possible to generate documentation directly from your PHP source code. With this you can provide your consumers with more information regarding the functionality embedded within your source and not just what is usable to them from your user interface.

For easy of use, the PhpDocumentor archive has been added to the repository and is located under /tools/ folder.

## Getting Started

The instruction is split into following parts:
1) Setting up config file
2) Adding comments to PHP code.
3) Running documentor from command line.
4) Fixing report errors.

## What to do?

### 1) Setting up config file
To automate the doc generation, a config file has to be created. The name of the config file has to be "phpdoc.dist.xml" and the file has to be located in the project root path.

Example of a phpdoc.dist.xml:

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<phpdoc>
  <parser>
    <target>./docs/phpDoc/cache</target>
  </parser>
    <transformer>
    <target>./docs/phpDoc/</target>
  </transformer>
  <files>
    <directory>./app/</directory>
    <ignore>tests/*</ignore>
    <!-- <ignore>app/Http/Middleware/*</ignore> -->
    <!-- <ignore>app/Providers/*</ignore> -->
    <ignore>app/Http/Controllers/_archive/*</ignore>
    <!-- <ignore>app/Notifications/*</ignore> -->
    <!-- <ignore>app/Exceptions/*</ignore> -->
    <!-- <ignore>app/Http/Kernel.php</ignore> -->
    <!-- <ignore>app/Http/Controllers/Controller.php </ignore> -->
    <!-- <ignore>app/Console/*</ignore> -->
  </files>
</phpdoc>
```


Everything is wrapped into the <phpdoc> tag. 

The <parser> section contains all settings related to the conversion of your project’s source to the intermediate structure format of phpDocumentor (structure.xml).

The <transform> section contains most settings related to the transformation of the intermediate structure format (structure.xml) to a human-readable set of documentation. This will be the output folder of the documentation.

The <files> section contains the main settings: <directory> for which files to document. <gnore> for which files/directories to ignore.

For more tags see: 
http://docs.phpdoc.org/references/configuration.html


### 2) Adding comments to PHP code.
Refer to following link for adding documentation blocks:
http://docs.phpdoc.org/guides/docblocks.html


### 3) Running documentor from command line.
Navigate to project root folder and run:

php ./tools/phpDocumentor.phar

This will automatically grab the config file and run the documentation. PhpDocumentor creates an additional reports folder for showing errors creating the documentation (e.g. summary missing, @param missing etc.).

You can find the reports in following folder: 

./tools/docs/phpDoc/reports/errors.html

## Troubelshooting Links
https://docs.phpdoc.org