# How to setup and use JSDoc

**Introduction:**

JSDoc is an API documentation generator for JavaScript, similar to Javadoc or phpDocumentor. You add documentation comments directly to your source code, right alongside the code itself. The JSDoc tool will scan your source code and generate an HTML documentation website for you.

JSDoc should be already installed with npm/node. If not, run "npm install".
You can also install JSDoc globally for using in other projects:
npm install -g jsdoc

## Getting Started

The instruction is split into following parts:
1) Setting up config file
2) Adding comments to JS code.
3) Running documentor from command line.


## What to do?

### 1) Setting up config file
To automate the doc generation, a config file has to be created. The name of the config file has to be "conf.json" and the file has to be located in the project root path.

Example of a conf.json:

```json
{
  "tags": {
    "allowUnknownTags": true
  },
  "source": {
    "include": ["./resources/assets/js"],
    "includePattern": ".+\\.js(doc|x)?$"
  },
  "plugins": [],
  "templates": {
      "cleverLinks": false,
      "monospaceLinks": true,
      "systemName": "remoteFileExplorer",
      "footer": "Team Laravel",
      "copyright": "2018",
      "navType": "vertical",
      "theme": "spacelab",
      "linenums": true,
      "collapseSymbols": true,
      "inverseNav": false
  },
  "opts": {
    "encoding": "utf8",
    "destination": "./docs/jsdoc",
    "recurse": true
  }
}
```

The tags section is for adding additional tags.

The source section is for defining which path and files to include. For includePattern regex can be used.

Plugin section for plugins.

Rest is more or less self explanatory, or refer to:
http://usejsdoc.org/about-configuring-jsdoc.html


### 2) Adding comments to JS code.
Refer to following link for adding documentation blocks:
http://usejsdoc.org/about-block-inline-tags.html


### 3) Running JSDoc from command line.
You can run JSDoc from node_modules folder like so:

./node_modules/.bin/jsdoc -c conf.json

This will create the documentation to the /docs/jsdoc/ folder.

## Troubelshooting Links
Run 

./node_modules/.bin/jsdoc --help

to see command helps.


http://usejsdoc.org