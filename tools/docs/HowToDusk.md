# How to use Dusk for Browser Testing

**Declarations:**

**Dusk:**

Laravel Dusk provides an expressive, easy-to-use browser automation and testing API.

## Getting Started

These instruction will get you started with Browser Testing with Dusk. After this instruction you should have Dusk installed and be able to run Browser Tests.

## What to do?

### Install Dusk

Add the laravel/dusk Composer dependency to your project:
Note: The version <2.0> is important

**`composer require --dev laravel/dusk 2.0`**

run the dusk:install Artisan command:
 
**`php artisan dusk:install`**

### Run Browser Tests

The Browser Tests can be found under tests/Browser

To run your tests, use the dusk Artisan command:

**`php artisan dusk`**

> Allowing you to only run the tests for a given group

> **`php artisan dusk --group=foo`**

### Make a new Browser Test

This command creates a new test in the tests folder:

**`php artisan dusk:make NameOfTest`**

#### Note

Since not every person uses the same url to go to the project's homepage (for example /localhost), you have to define your personal URL.

In your .env file under the variable APP_URL, write your path to your project home directory.

When writing tests use the variable like this 
**`$url = getenv('APP_URL');`**

See example UserLoginTest.php


## Troubelshooting

If error similar to this occurs:
_Facebook\WebDriver\Exception\NoSuchElementException: no such element: Unable to locate element..._

Try to install newest Chromedriver 

https://chromedriver.storage.googleapis.com/index.html?path=2.37/

Replace file chromedriver-win.exe or chromedriver-mac (depending on your opertating system) with new file.
File is located in vendor/laravel/dusk/bin


