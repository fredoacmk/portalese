# How to write Unit and Feature Test with Laravel


**Difference Unit vs. Feature Test**

By default, your application's tests directory contains two directories: Feature and Unit. Unit tests are tests that focus on a very small, isolated portion of your code. In fact, most unit tests probably focus on a single method. 
Feature tests may test a larger portion of your code, including how several objects interact with each other or even a full HTTP request to a JSON endpoint.

An ExampleTest.php file is provided in both the Feature and Unit test directories. 

## Getting Started

### Create a new Test

To create a new test case, use the make:test Artisan command.
Create a test in the Feature directory:
**`php artisan make:test UserTest`**
Create a test in the Unit directory:
**`php artisan make:test UserTest --unit`**

Once the test has been generated, you may define test methods as you normally would using PHPUnit.
Every method with the prefix "test" will be executed by phpunit if you run it. An example is automatically generated. You can change it and copy/edit it to make more tests.

#### Note

Since not every person uses the same url to go to the project's homepage (for example /localhost), you have to define your personal URL.

In your .env file under the variable APP_URL, write your path to your project home directory.

When writing tests use the variable like this 
**`$url = getenv('APP_URL');`**

### Run your Tests

run phpunit on the command line to run your tests. (use alt+f12 in phpstorm to open console)

**`php ./tools/phpunit7.1.4.phar`** or

**`phpunit`**
or if doesn't work
**`vendor/bin/phpunit`**

You should see an output similar to this:
```
PHPUnit 7.1.4 by Sebastian Bergmann and contributors.

..SSS....S.ISSSS....I.....                                        26 / 26 (100%)

Time: 17.22 seconds, Memory: 18.00MB

OK, but incomplete, skipped, or risky tests!
Tests: 26, Assertions: 43, Skipped: 8, Incomplete: 2.
```

The "." mean the test has passed, "S" means the test has been skipped, and "I" means the test is incomplete.
See the phpunit documentation for more detail on php unit testing. (https://phpunit.readthedocs.io/en/7.1/)

to run only the tests from one test file, add the path to your test file. 
For example:

**`vendor/bin/phpunit tests/Unit/someTestFile`**

## Important

On every commit tests will be run on the CI on Gitlab. 
See here: (https://gitlab.fhnw.ch/IP34-17vt_Leseportal/IP34-17vt_Leseportal/pipelines)

Please only make a commit if your tests pass!

## Further informations

(https://laravel.com/docs/5.6/testing)