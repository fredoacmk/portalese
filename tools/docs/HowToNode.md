# How to use Node and NPM (Node Package Manager)

**Declarations:**

**Node:**

Node is an ecosystem of open source libraries. With Node, you can run JavaScript on a server. You can find more in the internet.

**NPM:**

NPM is the "Node Package Manager" where you can install packages globally (using everywhere with command-line on the system, OS independent) or for project purposes only. There are packages which are essentials for some projects which have to be downloaded. All project related packages are downloaded into the "node_modules" folder. The packages.json defines all the packages needed. Once npm has downloaded all the packages, a packages.lock is generated. A "lock" file saves all the packages with the current version of each one in order to place the lock file on to a VCS for everyone to have the same version.

## Getting Started

These instruction will get you started with Node and NPM. After this instruction you should have Node installed.


## What to do?

### Install Node
Go to nodejs.org and download and install Version >8.9.4 as of 25. February 2018. After installation Node and NPM should be installed. Important (for Windows): If you get asked during installation to add Node to the systempath, do so. Otherwise you have to add it later yourself in order to use it in CMD.

### Test your environment
 
Type in console/powershell/bash/cmd/terminal: node -v
Type in console/powershell/bash/cmd/terminal: npm -v

In both cases, you should see a version output of the currently installed instance. If you don't, get some help.

## Getting a Node project to run

To get a Node project to run, you have to first download all the dependencies defined in the packages.json file. To do so, navigate in cmd/terminal to the project root folder (where package.json is) and run the command: npm install

This downloads all the depencencies. If you get errors, retry the command and see if it pops up again. Otherwise try to run cmd with admin privileges (Windows). If all that doesn't help, ask. Warnings can be ignored by now...

## Useful commands

Following commands are issued from the project root folder:

npm run watch - issues a watcher which compiles the files automatically after changes have been detected. After that, reload page to get new state.


## Troubelshooting

Ask.