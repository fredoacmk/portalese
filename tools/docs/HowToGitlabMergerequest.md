# How to file a merge request on Gitlab

A short tutorial on how to properly file a merge request, address merge conflicts and other common pitfalls to avoid.

## Prerequisites

You are working on a feature branch as per our workflow.
You have committed and pushed your latest additions.
You have gitlab opened in the browser, are logged in, and in our project space.

## Steps

* On the left side, hover over the icons, find "merge requests", and click that.
* If you see your latest commit at the top, click the blue button that says "create merge request"
* otherwise, file a merge request with the green button and fill in the details yourself:
 * Namely, select your feature branch as the source branch, and "develop" as your target branch.
* Assign our SA (Kenan) in the "assignee" field.
* Add a comment if the intent of the merge is not entierely obvious.
* Set "develop" as target branch.
* Check everything again.
* click "submit merge request".
* go check the newly submitted merge request. If there are "merge conflicts" go to section "merge conflicts". otherwise you are done.

## Pitfalls

* you have confused source and target branch: make sure that you merge _into_ develop (have "develop" as target)
* you accidentally submit a false request: go to the merge request, and click "close". This will not merge anything and close the request.

## Merge conflicts

If you have changes in your branch that conflict with changes done on (or merged into) the develop branch, gitlab will warn you of merge conflicts. This means your branch can not (yet) be merged into develop. To amend these conflicts, you must first merge develop back into your branch (exactly the other way around as a merge into develop!). To do this, `git checkout` your feature branch (on your machine), `git merge develop` to get all recent changes, and fix the resulting merge conflicts __locally__. Then commit your branch, push, and now you will see that your merge request can be merged without conflicts (because you have resolved them).

