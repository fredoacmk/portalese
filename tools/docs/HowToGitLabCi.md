# How to setup GitLab CI for continous integration

**Introduction:**

GitLab provides a tool out of the box for using CI. Easy said, one can prodive a .yaml in a branch and tell the runner server what to do with the repository. Example stages could be: Preparation (renaming files, copying, preparing), Installing Dependencies, Running Build, Testing.

**Definitions**

**.yaml:** File extension „YAML Ain’t Markup Language“ (previous „Yet Another Markup Language“).
**Runner Server:** The server which acts as the "processessor", "the doer", "Worker" etc. This server builds the software and runs the tests.
**Pipelines and Jobs:** A pipeline is a group of jobs that get executed in stages(batches). All of the jobs in a stage are executed in parallel (if there are enough concurrent Runners), and if they all succeed, the pipeline moves on to the next stage. More information: https://docs.gitlab.com/ee/ci/pipelines.html

## Getting Started

The instruction is split into following parts:
1) Setting up Runner Server
2) Setting up GitLab Runner
3) Prepare GitLab
4) Creating a .yaml file
a) Creating no passphrase private key
 

## What to do?

### 1) Setting up Runner server
Recommended OS is Centos 7 and the tutorial will be written with Centos specific commands. The commands should be easy to port to Ubuntu etc.

First, get libraries up to date with "sudo yum update -y". The -y flag hits automatically yes when prompted for install yes or no (Y/N).

Install nano with "sudo yum install nano -y".

**Install PHP v7.1:**

"sudo yum install wget -y"
"wget http://rpms.remirepo.net/enterprise/remi-release-7.rpm"

"sudo rpm -Uvh remi-release-7.rpm"

**Enable php71 repository which is disabled by default:**

"sudo yum install yum-utils -y"
"sudo yum-config-manager --enable remi-php71"

**Secondly, install PHP package:**

"sudo yum --enablerepo=remi,remi-php71 install php-fpm php-common -y"

**Install common modules:**

"sudo yum --enablerepo=remi,remi-php71 install php-opcache php-pecl-apcu php-cli php-pear php-pdo php-mysqlnd php-pgsql php-pecl-mongodb php-pecl-redis php-pecl-memcache php-pecl-memcached php-gd php-mbstring php-mcrypt php-xml"

"sudo yum install php-posix -y"

Check your instalaltion with "php -version"

**Install Node:**

Install necessary packages:
"sudo yum install curl -y"

Install Node.js and npm from the NodeSource repository:
"sudo yum install epel-release"
"curl --silent --location https://rpm.nodesource.com/setup_8.x | sudo bash -"
"sudo yum install nodejs -y"

Install build tools:
"sudo yum install gcc-c++ make"

Verify npm installation:
"node -v"
"npm -v"

**Install GitLab-Runner:**

Add GitLab's official repository:
"curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bash"

Install the latest version of GitLab Runner, or skip to the next step to install a specific version:
"sudo yum install gitlab-runner -y"

### 2) Setting up GitLab Runner
Once the server is ready and GitLab Runner installed, you can register your server to GitLab. First, in GitLab go to "Settings>CI / CD>Runner settings" and keep this page ready.

Run the following command on the server:
"sudo gitlab-runner register"

Enter your GitLab instance URL:
Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.fhnw.ch/)
(you should have the page open in GitLab).

Enter the token you obtained to register the Runner:

Enter a description for the Runner:

Enter the tags associated with the Runner, you can change this later in GitLab's UI:

Choose whether the Runner should pick up jobs that do not have tags, you can change this later in GitLab's UI (defaults to false):

Choose whether to lock the Runner to the current project, you can change this later in GitLab's UI. Useful when the Runner is specific (defaults to true):


Enter the Runner executor:
Please enter the executor: ssh, docker+machine, docker-ssh+machine, kubernetes, docker, parallels, virtualbox, docker-ssh, shell:

We choose SSH.

First enter the IP address of the server you are SSH'ed, Port 22, user is the one you log into, no ssh password (just enter), enter path to private key.

You should now see your runnder in "Settings>CI / CD>Runner settings".

### 3) Prepare GitLab (owner/master priviliges needed on GitLab)
In order to allow GitLab to SSH into your server, the credentials of the server (Private and Public key) have to be stored on GitLab.

First, go to "Settings>Repository>Deploy Keys" and enter as "Title" your user name of the server (e.g. root or centos) and paste in the servers public into the "Key" textare. Then, press "Add key".

Important: You need a private key with no passphrase. If there is no private key in path /home/USER.NAME/.ssh, or your key has a passphrase, refer to section a).

Go to "Settings>CI / CD>Secret variables" and enter as Key "SSH_PRIVATE_KEY" and as value paste in the private key of the server. Finally, press "Add new variable".

GitLab should now have access to your server for deploying.



### 4) Creating a .yaml file
The .yaml file describes exactly what the server should do. The commands can be triggered manually in section Pipelines, or will be triggered on every commit of a branch containing the .yaml file.

For detailed .yaml configuration, refer to official GitLab documentation:
https://docs.gitlab.com/ee/ci/yaml/

Example .yaml:

variables:
  MYSQL_DATABASE: leseportal
  MYSQL_ROOT_PASSWORD: ''
  
stages:
  \- prepare
  \- composer-install
  \- npm-install
  \- php-testing
  \- deploy-to-test

prepare:
  stage: prepare
  script:
  \- ls

composer-install:
  stage: composer-install
  script:
  \- mv composer.json.copy_rename composer.json
  \- mv .env.productive .env
  \- php ./tools/composer.phar install
  \- php ./tools/composer.phar dump-autoload
  \- php artisan config:cache
  \- php artisan key:generate
  
npm-install:
  stage: npm-install
  script:
  \- npm install
  \- npm run dev
  
php-testing:
  stage: php-testing
  script:
  \- ls
  
deyplo-to-test:
  stage: deploy-to-test
  script:
  \- ls
  

### a) Creating no passphrase private key
In order to avoid passphrase errors and complications, you can create a passphrase less rsa key. First, navigate to /.ssh/ and run following command.

ssh-keygen -b 2048 -t rsa -f id_rsa -q -N ""

This will generate the private key and the public key.


## Troubelshooting Links
https://www.hostinger.com/tutorials/how-to-install-lemp-centos7
https://docs.gitlab.com/runner/install/linux-repository.html
https://nodejs.org/en/download/package-manager/
https://docs.gitlab.com/ee/ci/examples/laravel_with_gitlab_and_envoy/index.html
