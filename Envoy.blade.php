@servers(['web' => 'leseport@leseportal.ch'])

@setup
    $repository = 'git@gitlab.fhnw.ch:IP34-17vt_Leseportal/IP34-17vt_Leseportal.git';
    $releases_dir = '/home/leseport/www/testing/';
@endsetup

@story('deploy')
    clean
    clone
    composer
    remove_files
@endstory

@task('clean')
    echo 'Cleaning path'
    rm -rf {{ $releases_dir }}
    mkdir {{ $releases_dir }}
@endtask

@task('clone')
    echo 'Cloning repository'
    cd {{ $releases_dir }}
    git clone -b master --depth 1 {{ $repository }} .
@endtask

@task('composer')
    echo 'Starting deployment'
    cd {{ $releases_dir }}
    mv composer.json.copy_rename composer.json
    mv .env_testing.leseportal .env
    php ./tools/composer.phar install --no-dev
    php ./tools/composer.phar dump-autoload
    php artisan config:cache
    php artisan key:generate
    php artisan migrate:refresh
    php artisan db:seed
    chmod -R 777 storage
@endtask

@task('remove_files')
    echo 'Remove not needed files'
    cd {{ $releases_dir }}
    rm -rf .git
    rm -rf .gitattributes
    rm -rf .gitignore
    rm -rf .gitlab-ci.yml
    rm -rf .env.example
    rm -rf .env.productive
    rm -rf .eslintrc.json
    rm -rf composer.lock.copy_rename
    rm -rf package-lock.json_rename
    rm -rf Envoy.blade.php
    rm -rf conf.json
    rm -rf conf.json
    rm -rf phpunit.xml
    rm -rf phpdoc.dist.xml
    rm -rf phpunit.dusk.xml
    rm -rf README.md
    rm -rf tests/
    rm -rf tools/docs/
    rm -rf tools/phpDocumentor.phar
    rm -rf tools/phpunit-7.1.4.phar
@endtask

