/** 
* This function runs when the DOM is ready.
* 
* @function jQuery ready()
*/
$(document).ready(function() {
    setupEventHandlers();
});

/** 
* This function registers all the EventListeners.
* 
* @function setupEventHandlers
* @returns {void}
*/
function setupEventHandlers() {
}