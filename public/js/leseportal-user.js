
/**
* This function runs when the DOM is ready.
* 
* @function jQuery ready()
*/
$(document).ready(function () {
    setupEventHandlers();
    userSidebar();
    populateSidebarInterest();
});


/****************************************************
 * global user functions
 * **************************************************/

/** 
* This function registers all the EventListeners.
* 
* @function setupEventHandlers
* @returns {void}
*/
function setupEventHandlers () {
}

/**
 * This function is for the user Sidebar menu.
 *
 * @function userSidebar
 * @returns {void}
 */
function userSidebar() {
    $("#sidebarCollapse").on("click", function () {
        $("#user-sidebar, #content").toggleClass("active");
        $(".collapse.in").toggleClass("in");
        $("a[aria-expanded=true]").attr("aria-expanded", "false");
    });
    // Highlight menu item in sidebar
    $("a").each(function() {
        if ($(this).prop("href") == window.location.href) {
            $(this).addClass("current");
        }
    });
}

/**
 * Populates user side bar with AreaOfInterest list.
 *
 * @function populateInterest
 * @returns {void}
 */
function populateSidebarInterest() {
    $.get( "/api/v1/interest", function(data) {
        var $navMenu = $("#user-seitenSubmenu");
        var interests = JSON.parse(data);
        $.each(interests, function(index) {
            var id = interests[index].idLebensbereich;
            var path = "/lesen/"+id;
            $navMenu.append($('<li />').append($('<a/>').text(interests[index].name).attr("href", path).attr("id", "interest-"+id)));
        });
    });
}

/****************************************************
 * user-dashboard functions
 * **************************************************/

/**
 * This function loads all user dashboard elements.
 *
 * @function loadDashboard
 * @returns {void}
 */
function loadDashboard() {
    $.get("/api/v1/userdashboard", function (data) {
        var $cardblock = $(".card-block"),
        $thumbnail = $(".thumbnail"),
        texts = JSON.parse(data);

        $cardblock.each(function(i, obj) {
            $(this).append($("<h4 />").text(texts[i].titel));
            $(this).append($("<p />").text(texts[i].lebensbereich));
            $(this).append($("<p />").text(texts[i].kompetenzstufe));
            $(this).attr("idLebensbereich", texts[i].idLebensbereich);
            $(this).attr("idSeite", texts[i].idSeite);
        });
        $thumbnail.css( 'cursor', 'pointer' );
        $thumbnail.click(function() {
            var idLebensbereich =  $(this).children("div").attr("idLebensbereich"),
            idSeite =  $(this).children("div").attr("idSeite"),
            path = '/lesen/'+idLebensbereich+'/'+idSeite;
            window.location.href = path;
        })
    });
    console.log("dashboard loaded!");
}

/****************************************************
 * user-area-of-interest functions
 * **************************************************/

/**
 * Sets the title for each area of interest page
 *
 * @function setTitleInterest
 * @returns {void}
 */
function setTitleInterest() {
    $.get( "/api/v1/interest", function(data) {
        var $title = $("h2.title");
        var path = window.location.pathname,
            parts = path.split("/"),
            last_part = parts[parts.length-1];
        var interests = JSON.parse(data);
        $.each(interests, function() {
            $title.text(interests[last_part-1].name);
        });
    });
}


/**
 * This function loads all user areaOfInterest elements.
 *
 * @function loadAreaOfInterest
 * @returns {void}
 */
function loadAreaOfInterest() {
    var path = window.location.pathname,
        parts = path.split("/"),
        last_part = parts[parts.length-1];

    $.get("/api/v1/userdashboard", function (data) {
        var $cardblock = $(".card-block"),
            $thumbnail = $(".thumbnail"),
            texts = JSON.parse(data);

        $thumbnail.css( 'cursor', 'pointer' );
        $thumbnail.click(function() {
            var idLebensbereich =  $(this).children("div").attr("idLebensbereich"),
                idSeite =  $(this).children("div").attr("idSeite");
            window.location.href ='/lesen/'+idLebensbereich+'/'+idSeite;
        });

        var counter = 0;
        $cardblock.each(function(index) {
            while (texts[counter] != undefined && last_part != texts[counter].idLebensbereich) {
                counter++;
            }

            if(texts[counter] != undefined) {
                $(this).append($("<h4 />").text(texts[counter].titel));
                $(this).append($("<p />").text(texts[counter].lebensbereich));
                $(this).append($("<p />").text(texts[counter].kompetenzstufe));
                $(this).attr("idLebensbereich", texts[counter].idLebensbereich);
                $(this).attr("idSeite", texts[counter].idSeite);
                counter++;
            } else {
                $thumbnail[index].remove();
                $cardblock[index].remove();
            }
        });
    });
}

/****************************************************
 * user-page-show functions
 * **************************************************/

/**
 * Displays the correct snippets to the according text.
 *
 * @function showSnippetSection
 * @returns {void}
 */
function showSnippets(idSeite) {
    path = "/api/v1/snippets/"+idSeite;

    $.get(path, function(data) {
        $.each(data, function (index) {
            snippettype = data[index].type;
            switch (snippettype) {
                case 'MC':
                    populateMultipleChoiceSnippet(index, idSeite);
                    break;
                case 'DD':
                    break;
                case 'TF':
                    populateTrueFalseSnippet(index, idSeite);
                    break;
                case 'LT':
                    break;
            }
        });
    });
    if ($("#show-snippets-section").children().length == 0) {
        $(".submit-block").addClass("hidden");
        $("hr").addClass("hidden");
    }
}

/**
 * This function loads the MultipleChoiceSnippet to the corresponding text.
 *
 * @function populateMultipleChoiceSnippet
 * @returns {void}
 */
function populateMultipleChoiceSnippet(index, idSeite){
    path = "/api/v1/snippets/"+idSeite;

    $.get(path, function(data){
        const numberOfAnswersAllowed = 5;
        let titleSnippettype = '<h2 class="title">Multiple Choice</h2>';
        let $resultsSection = $("#show-snippets-section");

        $resultsSection.append($(titleSnippettype));
        delete data[index].type;   // deletes element 'type' in array 'data'

        $.each(data[index], function (i) {
            let rowQuestion = '<h3>Frage '+( +i+1 )+'</h3> <div class="row"><div class="col-lg-5 col-md-7 col-sm-8 col-xs-12"><h4>'+data[index][i].frage+'</h4></div></div>';
            // let hiddenInput = '<input name="id_bla" type="hidden" value="'+data[index][i].id+'">';
            $resultsSection.append($(rowQuestion));
            // $resultsSection.append($(hiddenInput));

            for (let j = 1; j <= numberOfAnswersAllowed; j++) {
                let answers = "antwort"+j;
                if (data[index][i][answers] !=  null) {
                    let rowAnswer = '<div class="row"><div class="col-lg-5 col-md-7 col-sm-8 col-xs-12"><label class="control control--checkbox">'+data[index][i][answers]+'<input type="checkbox" name="MC '+data[index][i].id+"_"+j+'" value="checked"/><div class="control__indicator indicator-checkbox"></div></label></div></div>';
                    $resultsSection.append($(rowAnswer));
                }
            }
        });
    });
}

/**
 * This function loads the TrueFalseSnippet to the corresponding text.
 *
 * @function populateTrueFalseSnippet
 * @returns {void}
 */
function populateTrueFalseSnippet(index, idSeite) {
    path = "/api/v1/snippets/"+idSeite;

    $.get(path, function(data) {
        let titleSnippettype = '<h2 class="title">True or False</h2>';
        let $resultsSection = $("#show-snippets-section");
        let labels = '<div class="row icons"> <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12"><i class="fa fa-check fa-lg" aria-hidden="true"></i> <i class="fa fa-times fa-lg" aria-hidden="true"></i> </div></div>';

        $resultsSection.append($(titleSnippettype));
        $resultsSection.append($(labels));
        delete data[index].type;   // deletes element 'type' in array 'data'

        $.each(data[index], function (i) {
            let rowStatement = '<div class="row"> <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 statements"> <label class="control control--radio">  <input type="radio" name="TF '+data[index][i].id+'" id="'+data[index][i].id+'" value="selected true"/> <div class="control__indicator indicator-radio true"></div> </label> <label class="control control--radio"><input type="radio" name="TF '+data[index][i].id+'" id="'+data[index][i].id+'" value="selected false"/><div class="control__indicator indicator-radio false"></div> '+data[index][i].statement+'</label>  </div></div>';
            $resultsSection.append($(rowStatement));
        });
    });

}

/****************************************************
 * user-results functions
 * **************************************************/

/**
 * Displays the correct snippets to the according text.
 *
 * @function showResults
 * @returns {void}
 */
function showResults(combined) {
    var results = JSON.parse(combined);
    $.each(results, function (index) {
        let snippettype = results[index][0].snippetTypeId;
        switch (snippettype) {
            case 1:
                populateMultipleChoiceResults(combined, index);
                break;
            case 2:
                populateTrueFalseResults(combined, index);
                break;
            case 3:
                break;
            case 4:
                break;
        }
    });
}

/**
 * This function loads the MultipleChoice results to the corresponding text.
 *
 * @function populateMultipleChoiceResults
 * @returns {void}
 */
function populateMultipleChoiceResults(combined, index){
    let results = JSON.parse(combined);
    const numberOfAnswersAllowed = 5;
    let titleSnippettype = '<h2 class="title">Multiple Choice</h2>';
    let $resultsSection = $("#show-results-section");
    $resultsSection.append($(titleSnippettype));

    $.each(results[index], function (i) {

        let rowQuestion = '<h3>Frage '+( +i+1 )+'</h3> <div class="row"><div class="col-lg-5 col-md-7 col-sm-8 col-xs-12"><h4>'+results[index][i].frage+'</h4></div></div>';
        $resultsSection.append($(rowQuestion));

        for (let j = 1; j <= numberOfAnswersAllowed; j++) {
            let answer = "antwort"+j;
            if (results[index][i][answer] !=  null) {
                let rowAnswer;
                let correct = "richtig"+j;
                let evaluation = results[index][i][correct];

                // Depending on evaluation, add rowAnswer with different classes for css styling.
                switch (evaluation) {
                    case true:
                        rowAnswer = '<div class="row"><div class="col-lg-5 col-md-7 col-sm-8 col-xs-12"><label class="control control--checkbox">'+results[index][i][answer]+'<input type="checkbox" name="MC '+results[index][i].id+"_"+j+'" value=""/><div class="indicator-checkbox checkbox-checked-true"></div></label></div></div>';
                        break;
                    case false:
                        rowAnswer = '<div class="row"><div class="col-lg-5 col-md-7 col-sm-8 col-xs-12"><label class="control control--checkbox">'+results[index][i][answer]+'<input type="checkbox" name="MC '+results[index][i].id+"_"+j+'" value=""/><div class="indicator-checkbox checkbox-checked-false"></div></label></div></div>';
                        break;
                    case 0:
                        rowAnswer = '<div class="row"><div class="col-lg-5 col-md-7 col-sm-8 col-xs-12"><label class="control control--checkbox">'+results[index][i][answer]+'<input type="checkbox" name="MC '+results[index][i].id+"_"+j+'" value=""/><div class="indicator-checkbox checkbox-notChecked-false"></div></label></div></div>';
                        break;
                    case 1:
                        rowAnswer = '<div class="row"><div class="col-lg-5 col-md-7 col-sm-8 col-xs-12"><label class="control control--checkbox">'+results[index][i][answer]+'<input type="checkbox" name="MC '+results[index][i].id+"_"+j+'" value=""/><div class="indicator-checkbox checkbox-notChecked-true"></div></label></div></div>';
                        break;
                }
                $resultsSection.append($(rowAnswer));
            }
        }
    });
}

/**
 * This function loads the TrueFalseResults to the corresponding text.
 *
 * @function populateTrueFalseResults
 * @returns {void}
 */
function populateTrueFalseResults(combined, index) {
    let results = JSON.parse(combined);

    let titleSnippettype = '<h2 class="title">True or False</h2>';
    let $resultsSection = $("#show-results-section");
    let labels = '<div class="row icons"> <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12"><i class="fa fa-check fa-lg" aria-hidden="true"></i> <i class="fa fa-times fa-lg" aria-hidden="true"></i> </div></div>';

    $resultsSection.append($(titleSnippettype));
    $resultsSection.append($(labels));

    $.each(results[index], function (i) {
        let rowStatement;
        let wahr = results[index][i].richtig;
        let correct = results[index][i].correct;
        if (wahr && correct) {
            rowStatement = '<div class="row"> <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 statements"> <label class="control control--radio">  <input type="radio" name="TF '+results[index][i].id+'" id="'+results[index][i].id+'" value="selected true"/> <div class="indicator-radio true radio-checked-true"></div> </label> <label class="control control--radio"><input type="radio" name="TF '+results[index][i].id+'" id="'+results[index][i].id+'" value="selected false"/><div class="indicator-radio false radio-notChecked-false"></div> '+results[index][i].statement+'</label>  </div></div>';
        } else if (!wahr && correct) {
            rowStatement = '<div class="row"> <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 statements"> <label class="control control--radio">  <input type="radio" name="TF '+results[index][i].id+'" id="'+results[index][i].id+'" value="selected true"/> <div class="indicator-radio true radio-notChecked-false"></div> </label> <label class="control control--radio"><input type="radio" name="TF '+results[index][i].id+'" id="'+results[index][i].id+'" value="selected false"/><div class="indicator-radio false radio-checked-true"></div> '+results[index][i].statement+'</label>  </div></div>';
        } else if (!wahr && !correct) {
            rowStatement = '<div class="row"> <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 statements"> <label class="control control--radio">  <input type="radio" name="TF '+results[index][i].id+'" id="'+results[index][i].id+'" value="selected true"/> <div class="indicator-radio true radio-checked-false"></div> </label> <label class="control control--radio"><input type="radio" name="TF '+results[index][i].id+'" id="'+results[index][i].id+'" value="selected false"/><div class="indicator-radio false radio-notChecked-true"></div> '+results[index][i].statement+'</label>  </div></div>';
        } else if (wahr && !correct) {
            rowStatement = '<div class="row"> <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 statements"> <label class="control control--radio">  <input type="radio" name="TF '+results[index][i].id+'" id="'+results[index][i].id+'" value="selected true"/> <div class="control__indicator indicator-radio true radio-notChecked-true"></div> </label> <label class="control control--radio"><input type="radio" name="TF '+results[index][i].id+'" id="'+results[index][i].id+'" value="selected false"/><div class="control__indicator indicator-radio false radio-checked-false"></div> '+results[index][i].statement+'</label>  </div></div>';
        }
        $resultsSection.append($(rowStatement));
    });
}