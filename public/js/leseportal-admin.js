/****************************************************
 * Functions for the pages view
 * **************************************************/
/****************************************************
 * Functions for the create-page view
 * **************************************************/
/****************************************************
 * Functions for the area-of-interest view
 * **************************************************/
/****************************************************
 * Functions for the skill-level view
 * **************************************************/
/****************************************************
 * Functions for the snippets view
 * **************************************************/
/****************************************************
 * Functions for the create-snippet view
 * **************************************************/



/**
* German translation for datatables.
* 
* @constant
* @type {json}
*/
const datatableLang = {
    language: {
        "sEmptyTable":   	"Keine Daten in der Tabelle vorhanden",
        "sInfo":         	"_START_ bis _END_ von _TOTAL_ Einträgen",
        "sInfoEmpty":    	"0 bis 0 von 0 Einträgen",
        "sInfoFiltered": 	"(gefiltert von _MAX_ Einträgen)",
        "sInfoPostFix":  	"",
        "sInfoThousands":  	".",
        "sLengthMenu":   	"_MENU_ Einträge anzeigen",
        "sLoadingRecords": 	"Wird geladen...",
        "sProcessing":   	"Bitte warten...",
        "sSearch":       	"Suchen",
        "sZeroRecords":  	"Keine Einträge vorhanden.",
        "oPaginate": {
            "sFirst":    	"Erste",
            "sPrevious": 	"Zurück",
            "sNext":     	"Nächste",
            "sLast":     	"Letzte"
        },
        "oAria": {
            "sSortAscending":  ": aktivieren, um Spalte aufsteigend zu sortieren",
            "sSortDescending": ": aktivieren, um Spalte absteigend zu sortieren"
        }
    }
};

/** 
* This function runs when the DOM is ready.
* 
* @function jQuery ready()
*/
$(document).ready(function() {
    setupEventHandlers();
    adminSidebar();
});

/** 
* This function registers all the EventListeners.
* 
* @function setupEventHandlers
* @returns {void}
*/
function setupEventHandlers() {

    // This is only a snippet example for jQuery POST request. Will be deleted after usage.
    $("#saveText").click(function() {
        $.ajax({
            type: "POST",
            url:"/api/v1/savetext",
            data : $("#myForm").serialize(),
            error:  (data) => {
                alert("could not save!");
            },
            success: (data) => {
                //location.reload();
                alert(data);
            }
        });
    });
    
}

/** 
* This function is for the admin Sidebar menu.
* 
* @function adminSidebar
* @returns {void}
*/
function adminSidebar() {
    $("#sidebarCollapse").on("click", function () {
        $("#admin-sidebar, #content").toggleClass("active");
        $(".collapse.in").toggleClass("in");
        $("a[aria-expanded=true]").attr("aria-expanded", "false");
    });
    // Highlight menu item in sidebar
    $("a").each(function() {
        if ($(this).prop("href") == window.location.href) {
            $(this).addClass("current");
        }
    });
}

/**
 * Loads the datatable for columns filtering.
 * 
 * @function loadDatatable
 * @param {int} id of element.
 * @returns {void}
 */
function loadDatatable() {
    $("#overViewTable").DataTable(datatableLang);

    $("#body-container").show();
    $("#isLoading").hide();

}

/** 
* Populates Interest selection option list.
* 
* @function populateList
* @returns {void}
*/
function populateInterest() {
    $.get( "/api/v1/interest", function(data) {
        var $dropdown = $("#interest");
        var interests = JSON.parse(data);

        $.each(interests, function(index) {
            $dropdown.append($("<option />").val(interests[index].idLebensbereich).text(interests[index].name));
        });
    });
}

/** 
* Populates Competence selection option list.
* 
* @function populateCompetence
* @returns {void}
*/
function populateCompetence() {
    $.get( "/api/v1/competence", function(data) {
        var $dropdown = $("#competence");
        var competence = JSON.parse(data);

        $.each(competence, function(index) {
            $dropdown.append($("<option />").val(competence[index].idKompetenzstufe).text(competence[index].level));
        });
    });
}

/** 
* Populates the pages in admin pages overview.
* 
* @function populatePagesOverview
* @returns {void}
*/
function populatePagesOverview() {
    $("#body-container").hide();
    $("#isLoading").show();
    
    $.get( "/api/v1/pagesoverview", function(data) {
        var $list = $("#overViewTable tbody");

        data.forEach(element => {
            $list.append(
                "<tr>" +
                    "<td>"
                    +element.titel
                    +"</td><td>"
                    +element.lebensbereich
                    +"</td><td>"
                    +element.kompetenzstufe
                    +"</td><td>"
                    +element.updated_at
                    +"</td><td>"
                    +"<i class=\"icon fa fa-eye show_"+element.idSeite+"\"></i>"
                    +"<i class=\"icon fa fa-pencil edi_"+element.idSeite+"\"></i>"
                    +"<i class=\"icon fa fa-trash del_"+element.idSeite+"\"></i>"
                    +"</td>" +
                "</tr>");


            $(".show_"+element.idSeite).click(function() {
                window.location.href = "/admin/seiten/"+element.idSeite;
            });

            $(".edi_"+element.idSeite).click(function() {
                window.location.href = "/admin/seiten/"+element.idSeite+"/bearbeiten";
            });

            $(".del_"+element.idSeite).click(function() {

                if (confirm("Sind Sie sicher, dass Sie dieses Seite löschen möchten?\nAlle Seiten mit der selben ID werden dauerhaft gelöscht!")) {
                    $.ajax({
                        type: "DELETE",
                        url:"/admin/seiten/"+element.idSeite+"/delete",
                        data : element.idSeite,
                        error:  (data) => {
                            alert("Fehler aufgetreten.");
                        },
                        success: (data) => {
                            location.reload();
                        }
                    });
                }
            });
        });
    });
    loadDatatable();
}


/** 
* Populates the snippets in admin snippets overview.
* 
* @function populatePagesOverview
* @returns {void}
*/
function populateSnippetsOverview() {
    $("#body-container").hide();
    $("#isLoading").show();

    $.get( "/api/v1/snippetsoverview", function(data) {
        // data = array of objects: data[0] = 1st object, data[1] = 2nd etc.
        var $list = $("#overViewTable tbody");

        data.forEach(all => {
            all.forEach(element => {
                $list.append(
                    "<tr><td>"
                    +element.text_title
                    +"</td><td>"
                    +element.snippet_type
                    +"</td><td>"
                    +element.snippet_idSnippet
                    +"</td><td>"
                    +element.created_at
                    +"</td><td>"
                    //+"<a href=\"/snippets/"+element.snippet_idSnippet+"/bearbeiten\"><i class=\"fa fa-pencil\"></i></a>"
                    //+"<a href=\"/snippets/"+element.snippet_idSnippet+"\"><i class=\"fa fa-trash\"></i></a>"
                    +"<i class=\"icon fa fa-eye show_"+element.snippetTypeId+"_"+element.snippet_idSnippet+"\"></i>"
                    +"<i class=\"icon fa fa-pencil edi_"+element.snippetTypeId+"_"+element.snippet_idSnippet+"\"></i>"
                    +"<i class=\"icon fa fa-trash del_"+element.snippetTypeId+"_"+element.snippet_idSnippet+"\"></i>"
                    +"</td></tr>");
    
                $(".show_"+element.snippetTypeId+"_"+element.snippet_idSnippet).click(function() {
                    window.location.href = "/admin/snippets/"+element.snippetTypeId+"/"+element.snippet_idSnippet;
                });
    
                $(".edi_"+element.snippetTypeId+"_"+element.snippet_idSnippet).click(function() {
                    //alert("EDIT_" + element.snippet_idSnippet);
                    window.location.href = "/admin/snippets/"+element.snippetTypeId+"/"+element.snippet_idSnippet+"/bearbeiten";
                });
    
                $(".del_"+element.snippetTypeId+"_"+element.snippet_idSnippet).click(function() {
                    // alert("DELETE_" + element.snippet_idSnippet);
    
                    if (confirm("Sind Sie sicher, dass Sie dieses Snippet löschen möchten?\nAlle Snippets mit der selben ID werden dauerhaft gelöscht!")) {
                        $.ajax({
                            type: "DELETE",
                            url:"/admin/snippets/"+element.snippetTypeId+"/"+element.snippet_idSnippet+"/delete",
                            data : element.snippet_idSnippet,
                            error:  (data) => {
                                alert("Fehler aufgetreten.");
                            },
                            success: (data) => {
                                location.reload();
                            }
                        });
                    }
                });
            });
        });

    });
    loadDatatable();
}

/**
 * Populates area of interest table.
 *
 * @function populateAreaOfInterest
 * @returns {void}
 */
function populateAreaOfInterest() {
    $("#body-container").hide();
    $("#isLoading").show();

    $.get( "/api/v1/areaofinterestoverview", function(data) {
        // data = array of objects: data[0] = 1st object, data[1] = 2nd etc.
        var jSonData = JSON.parse(data); //jSonDate is parsed from array
        var $list = $("#overViewTable tbody");

        jSonData.forEach(element => {
            $list.append(
                "<tr><td>"
                +element.name
                +"</td><td>"
                +element.beschreibung
                +"</td><td>"
                //+"<a href=\"/snippets/"+element.snippet_idSnippet+"/bearbeiten\"><i class=\"fa fa-pencil\"></i></a>"
                //+"<a href=\"/snippets/"+element.snippet_idSnippet+"\"><i class=\"fa fa-trash\"></i></a>"
                +"<i class=\"icon fa fa-pencil edi_"+element.idLebensbereich+"\"></i>"
                +"<i class=\"icon fa fa-trash del_"+element.idLebensbereich+"\"></i>"
                +"</td></tr>");
    
            $(".edi_"+element.idLebensbereich).click(function() {
                //alert("EDIT_" + element.snippet_idSnippet);
                window.location.href = "/admin/lebensbereiche/"+element.idLebensbereich+"/bearbeiten";
            });
    
            $(".del_"+element.idLebensbereich).click(function() {
                // alert("DELETE_" + element.snippet_idSnippet);
    
                if (confirm("Sind Sie sicher, dass Sie diesen Lebensbereich löschen möchten?")) {
                    $.ajax({
                        type: "DELETE",
                        url:"/admin/lebensbereiche/"+element.idLebensbereich+"/delete",
                        data : element.idLebensbereich,
                        error:  (data) => {
                            alert("Fehler aufgetreten.");
                        },
                        success: (data) => {
                            location.reload();
                        }
                    });
                }
            });
        });
    });
    loadDatatable();
}

/****************************************************
 * Functions for the create snippet view
 * **************************************************/

/**
 * Populates selection with snippettypes.
 *
 * @function populateSnippettype
 * @returns {void}
 */
function populateSnippettype() {
    $.get( "/api/v1/getsnippetdropdown", function(data) {
        var $dropdown = $("#snippettype");
        var snippettype = JSON.parse(data);

        $dropdown.append($("<option selected disabled/>").text("Auswählen"));
        $.each(snippettype, function(key) {
            $dropdown.append($("<option />").text(snippettype[key]).val(key));
        });
    });
}

/**
 * Filters the list according to input.
 * 
 * @function searchFunction
 * @param {int} id of element.
 * @returns {void} shows founded names directly at the top of the row
 */
function searchFunction(id){
    // Declare variables
    var input, filter, table, tr, td, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    // overViewTable==id of table
    table = document.getElementById(id);
    tr = table.getElementsByTagName("tr");

    for (i = 0; i < tr.length; i++) {
        // .getElementsByTagName("td")[0]==first column, 1==second column
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                td = tr[i].getElementsByTagName("td")[1];
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                }else{
                    tr[i].style.display = "none";
                }
            }
        }
    }
}

/**
 * Populates competence table.
 *
 * @function populateCompetenceOverview
 * @returns {void}
 */
function populateCompetenceOverview() {
    $("#body-container").hide();
    $("#isLoading").show();
    
    $.get( "/api/v1/adminskilllevel", function(data) {
        // data = array of objects: data[0] = 1st object, data[1] = 2nd etc.
        var jSonData = JSON.parse(data); //jSonDate is parsed from array
        var $list = $("#overViewTable tbody");

        jSonData.forEach(element => {
            $list.append(
                "<tr><td>"
                +element.level
                +"</td><td>"
                +element.beschreibung
                +"</td><td>"
                +"<i class=\"icon fa fa-pencil edi_"+element.idKompetenzstufe+"\"></i>"
                +"<i class=\"icon fa fa-trash del_"+element.idKompetenzstufe+"\"></i>"
                +"</td></tr>");

            $(".edi_"+element.idKompetenzstufe).click(function() {
                //alert("EDIT_" + element.snippet_idSnippet);
                window.location.href = "/admin/kompetenzstufe/"+element.idKompetenzstufe+"/bearbeiten";
            });

            $(".del_"+element.idKompetenzstufe).click(function() {
                // alert("DELETE_" + element.snippet_idSnippet);

                if (confirm("Sind Sie sicher, dass Sie diese Kompetenzstufe löschen möchten?")) {
                    $.ajax({
                        type: "DELETE",
                        url:"/admin/kompetenzstufe/"+element.idKompetenzstufe+"/delete",
                        data : element.idKompetenzstufe,
                        error:  (data) => {
                            alert("Fehler aufgetreten.");
                        },
                        success: (data) => {
                            location.reload();
                        }
                    });
                }
            });
        });
    });
    loadDatatable();
}
/*
 * Populates list with all pages.
 *
 * @function populatePagesCreateSnippet
 * @returns {void}
 */
function populatePagesCreateSnippet() {
    $.get( "/api/v1/pagesoverview", function(data) {
        var $textList = $("#snippetTextList");
        $.each(data, function (index) {
            $textList.append($("<a/>").attr("name", data[index].idSeite).attr("value", data[index].idSeite).attr("class", "list-group-item list-group-item-action").text(data[index].titel));
        });

        $('.list-group-item').on('click', function() {
            var $this = $(this);
            $('.active').removeClass('active');
            $this.toggleClass('active');
            $("#pages_input_hidden").val($($this).attr("value"));
        });
    });
}

/**
 * Displays the correct snippet section according to the selection made.
 *
 * @function showSnippetSection
 * @returns {void}
 */
function showSnippetSection() {
    $('#snippettype').change(function() {
        questionCounter = 1;

        $("#create-snippet-section").empty();
        switch ($(this).val()) {
            case 'MC':
                populateCreateMultipleChoice();
                break;
            case 'DD':
                break;
            case 'TF':
                populateCreateTrueFalse();
                break;
            case 'LT':
                break;
        }
    });
}

/**
 * Populates section "create multiple choice snippet".
 *
 * @function populateCreateMultipleChoice
 * @returns {void}
 */
var questionCounter = 1;
function populateCreateMultipleChoice() {
    var $multipleChoiceSection = $("#create-snippet-section");
    var btnQuestion = '<div class="leseportal-space" id="btn-question-block"><div class="row"><div class="col-lg-3 col-mg-3 col-sm-3 col-xs-12"><a onclick="addQuestion()" class="btn btn-default">Frage hinzufügen</a></div></div></div>';
    $multipleChoiceSection.append($(btnQuestion));
    createQuestionBlock();

}
function createQuestionBlock() {
    var question = '<div class="leseportal-space"><div class="row"><div class="col-lg-2 col-md-2 col-sm-3 col-xs-12"><p> Frage '+questionCounter+' </p></div><div class="col-lg-7 col-md-7 col-sm-7 col-xs-12"><input type="text" class="form-control" id="usr" name="q'+questionCounter+'" required></div></div></div>';
    var answer = '<div class="leseportal-space" id="q'+questionCounter+'_a1"><div class="row"><div class="col-lg-2 col-md-2 col-sm-3 col-xs-12"><p> Antwort 1 </p></div><div class="col-lg-7 col-md-7 col-sm-7 col-xs-10"><input type="text" class="form-control" id="usr" name="q'+questionCounter+'_a1" required></div><div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"><input type="checkbox" name="q'+questionCounter+'_a1_richtig" value="q'+questionCounter+'_a1"><br></div></div></div>';
    var btnAnswer = '<div class="leseportal-space"><div class="row"><div class="col-lg-3 col-mg-3 col-sm-3 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-3"><a onclick="addAnswer($(this))" class="btn btn-default">Antwort hinzufügen</a></div></div></div>';
    var questionBlock = '<div class="question-block" id="question'+questionCounter+'">'+question+answer+btnAnswer+'</div>';
    $(questionBlock).insertBefore('#btn-question-block');
    questionCounter++;
}

function addAnswer(element) {
    var $currentQuestionBlock = $(element).closest(".question-block");

    var oldID = $($currentQuestionBlock).children().last().prev().attr('id');
    //console.log("oldID: "+ oldID);

    var lastChar = oldID.slice(-1);
    var newNumber = +lastChar + 1;
    var newID = oldID.slice(0, -1) + newNumber;
    //console.log("newID: "+newID);

    if (newNumber <= 5) {
        var current = document.getElementById(oldID);
        var newAnswer = '<div class="leseportal-space" id="' + newID + '"><div class="row"><div class="col-lg-2 col-md-2 col-sm-3 col-xs-12"><p>Antwort '+newNumber+'</p></div><div class="col-lg-7 col-md-7 col-sm-7 col-xs-10"><input type="text" class="form-control" id="usr" name="'+newID+'" required></div><div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"><input type="checkbox" name="'+newID+'_richtig" value="'+newID+'"><br></div></div></div>';

        $(newAnswer).insertAfter(current);
    }
}

function addQuestion() {
    createQuestionBlock();
}
/**
 * Populates section "create true or false snippet".
 *
 * @function populateTrue-False
 * @returns {void}
 */
function populateCreateTrueFalse(){

    var $TrueFalseSection = $("#create-snippet-section");
    var btnQuestion =
        '<div class="leseportal-space" id="btn-question-block">' +
        '<div class="col-lg-3 col-mg-3 col-sm-3 col-xs-12">' +
        '<a onclick="addQuestionTF()" class="btn btn-default">' +
        'Aussage hinzufügen' +
        '</a></div></div>';
    var btnQuestionDelete =
        '<div class="leseportal-space" id="btn-question-block">' +
        '<div class="row"><div class="col-lg-3 col-mg-3 col-sm-3 col-xs-12">' +
        '<a onclick="deleteQuestionTF()" class="btn btn-default">' +
        'Delete question' +
        '</a></div></div></div>';

    $TrueFalseSection.append($(btnQuestion),$(btnQuestionDelete));
    createQuestionBlockTF();
}
function createQuestionBlockTF() {
    var Statement = '<div class="leseportal-space" id="q'+questionCounter+'_a1">' +
        '<div class="row" id="'+questionCounter+'"><div class="col-lg-2 col-md-2 col-sm-3 col-xs-12"><p> Aussage '+questionCounter+' </p></div>' +
        '<div class="col-lg-7 col-md-7 col-sm-7 col-xs-10">' +
        '<input type="text" class="form-control" id="usr" name="q'+questionCounter+'" required></div>' +
        '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"><input type="radio" name="q'+questionCounter+'_richtig" value="q'+questionCounter+'">' +
        '<br></div></div></div>';
    var questionBlock = '<div class="question-block" id="question'+questionCounter+'">'+Statement+'</div>';
    $(questionBlock).insertBefore('#btn-question-block');
    questionCounter++;
}
function addQuestionTF() {
    createQuestionBlockTF();
}

/*
 * Use delete button to delete a question in the true and false snippet
  * */
function deleteQuestionTF() {
    questionCounter--;
    $('#question'+questionCounter).remove();
}
/**
 * Use the cancel button on Area of Interest page to return to the overview page
 *
 * @function return to area of interest overview page if text fields are empty
 * @returns {void}
 */
function registerCancelButtonAreaOfInterest(idTitle, idContent, idBtn) {
    $(idBtn).click(function() {

        if($(idTitle).val() || $(idContent).val()) {

            if(confirm("Sind Sie sicher, dass Sie alles Verwerfen wollen?")){
                window.location.href='/admin/lebensbereiche';
            }
        } else {
            window.location.href='/admin/lebensbereiche';
        }
    })
}

/**
 * Use the cancel button on article pages to return to the overview page
 *
 * @function return to article overview page if text fields are empty
 * @returns {void}
 */
function registerCancelButtonArticle(idTitle, idContent, idBtn) {
    $(idBtn).click(function() {

        if($(idTitle).val() || CKEDITOR.instances['article-ckeditor'].getData()) {

            if(confirm("Sind Sie sicher, dass Sie alles Verwerfen wollen?")){
                window.location.href='/admin/seiten';
            }
        } else {
            window.location.href='/admin/seiten';
        }
    })
}

/**
 * Populates dashboard of the admin.
 *
 * @function populateDashboardAdmin
 * @returns {void}
 */
function populateDashboardAdmin() {
    $.get( "/api/v1/admindashboard", function(data) {
        $DashboardCounts = JSON.parse(data);
        $("#admins").append($DashboardCounts.adminCount);
        $("#users").append($DashboardCounts.userCount);
        $("#snippets").append($DashboardCounts.snippetsCount);
        $("#lb").append($DashboardCounts.lebensbereicheCount);
        $("#seiten").append($DashboardCounts.siteCount);
    });
}

/**
 * Use the cancel button on Snippet page to return to the Snippet overview page
 *
 * @function return to snippet overview page
 * @returns {void}
 */
function registerCancelButtonSnippet(idBtn) {
    $(idBtn).click(function () {

        if (confirm("Sind Sie sicher, dass Sie alles Verwerfen wollen?")) {
            window.location.href = '/admin/snippets';
        }
    })
}

/**
 * Use the cancel button on Skill level page to return to the overview page
 *
 * @function return to overview page if text fields are empty
 * @returns {void}
 */
function registerCancelButtonSkillLevel(idTitle, idContent, idBtn) {
    $(idBtn).click(function() {

        if($(idTitle).val() || $(idContent).val()) {

            if(confirm("Sind Sie sicher, dass Sie alles Verwerfen wollen?")){
                window.location.href='/admin/kompetenzstufe';
            }
        } else {
            window.location.href='/admin/kompetenzstufe';
        }
    })
}

/**
 * Use the cancel button on article edit pages to return to the overview page
 *
 * @function return to article overview page
 * @returns {void}
 */
function registerCancelButtonEditArticle(idBtn) {
    $(idBtn).click(function() {

        if (confirm("Mit dem Bestätigen gehen alle Änderungen verloren.")) {
            window.location.href = '/admin/seiten';
        }
    })
}

/**
 * Use the cancel button on area of interest edit pages to return to the overview page
 *
 * @function return to area of interest overview page
 * @returns {void}
 */
function registerCancelButtonEditAreaOfInterest(idBtn) {
    $(idBtn).click(function () {

        if (confirm("Mit dem Bestätigen gehen alle Änderungen verloren.")) {
            window.location.href = '/admin/lebensbereiche';
        }
    })
}

/**
 * Use the cancel button on skill level edit pages to return to the overview page
 *
 * @function return to skill level overview page
 * @returns {void}
 */
function registerCancelButtonSkillLevel(idBtn) {
    $(idBtn).click(function () {

        if (confirm("Mit dem Bestätigen gehen alle Änderungen verloren.")) {
            window.location.href = '/admin/kompetenzstufe';
        }
    })
}