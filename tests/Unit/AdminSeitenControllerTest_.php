<?php
/**
 * Created by PhpStorm.
 * User: Königin
 * Date: 02/01/2018
 * Time: 16:41
 */

use App\Http\Controllers\AdminSeitenController;
use App\Kompetenzstufe;
use App\Lebensbereich;
use App\Seite;
use Illuminate\Http\Request;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware; //maybe delete again

class AdminSeitenControllerTest extends TestCase
{
    private $request;

    protected function setUp()
    {
        $this->request = new Request();
   }

    protected function tearDown()
    {
        $this->request = NULL;
   }

   /*
    * testing method titleSize
    * boundary values: valide: 1-80, invalid:  0, 81
    * @dataProvider trueTitleSizeDataProvider
    * @dataProvider falseTitleSizeDataProvider
    */
   public function trueTitleSizeDataProvider(){
       return array(
           array(1),
           array(80)
       );
   }
    public function testTitleSize($trueTitleSizeData)
    {
        $resultTitleSize=$this->request->titleSize($trueTitleSizeData);
        $this->assertTrue($resultTitleSize);
    }
    public function testTitleSizeZero()
    {
        $resultTitelSize=$this->request->titleSize(0);
        //todo  change assert/ expectException
        $this->expectException(InvalidArgumentException::class);
    }
    public function testTitleSizeOversize()
    {
        $resultTitelSize=$this->request->titleSize(81);
        //todo  change assert/ expectException
        $this->expectException(InvalidArgumentException::class);
    }


    /*
    * testing method textSize
    * boundary values: valide: 1-10'000, invalid:  0, 10'001
    * @dataProvider trueTextSizeDataProvider
    */
    public function trueTextSizeDataProvider(){
        return array(
            array(1),
            array(10000)
        );
    }
    public function testTextSize($trueTextSizeData)
    {
        $resultTextSize=$this->request->textSize($trueTextSizeData);
        $this->assertTrue($resultTextSize);
    }

    public function testTextSizeZero()
    {
        $resultTextSize=$this->request->textSize(0);
        //todo  change assert/ expectException
        $this->expectException(InvalidArgumentException::class);
    }
    public function testTextSizeOversize()
    {
        $resultTextSize=$this->request->textSize(10001);
        //todo  change assert/ expectException
        $this->expectException(InvalidArgumentException::class);
    }


    public function testSaveButton(){
        $admin1=factory(AdminsTableSeeder::class)->create();

        $response=$this->actingAs($admin1,'Testing12345')
        ->get('/admin/seiten/erstellen');
        //$this->click(save());
        //todo: function in Route, Rout::get('feedback',function(){return "Seite wurde gespeichert";});
        //$this->see('Seite wurde gespeichert');
        //todo: in php, <a href="/feedback">save</a>
        //$this->seePagels('/feedback');
    }
}
