<?php

namespace Tests\Feature;


use App\Admin;
use App\User;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Facade;
use phpDocumentor\Reflection\Types\Boolean;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
class HttpAdminPageTest extends TestCase
{
    private $valid_user;
    private $bad_user;
    private $ADMINDRIVER = 'admin'; //Driver) that has admin privileges
    private $REDIRECT = 302; //HTTP codes
    private $SUCCESS = 200;

    public function SetUp(){
        parent::SetUp();
        $this->valid_user = factory(Admin::class)->create();
        $this->bad_user = factory(User::class)->create();
    }

    /**
     * Helper function for checking if a link returns the expected status (optionally with logged in user and appropriate guard as driver or not)
     * Logs out again if a user is supplied.
     * @param string $uri
     * @param int $expectStatus
     * @param Model $asUser
     * @param String $driver
     */
    public function checkResponse(string $uri, int $expectStatus, Model $asUser = null, String $driver = '')
    {
        /** @var Boolean $shouldLogout used to check if we have to log out again at the end.*/
        $shouldLogout = false;
        if (! is_null($asUser))
        {
            $shouldLogout = true;
            if (str_contains($driver, 'admin'))
            {
                $this->be($asUser, $driver);
            }
            else
            {
                $this->be($asUser);
            }
        }
        $this->get($uri)->assertStatus($expectStatus);
        if($shouldLogout)
        {
            Auth::logout();
        }

    }

    /**
     * Checks if a page given by uri is accessible, and only accessible, by an authenticated admin and redirects otherwise
     * @param string $uri
     */
    public function checkAdminPrivilege(string $uri)
    {
        $this->checkResponse($uri, $this->REDIRECT);
        $this->checkResponse($uri, $this->REDIRECT, $this->bad_user);
        $this->checkResponse($uri, $this->SUCCESS, $this->valid_user, $this->ADMINDRIVER);
    }

    /**
     * Testing the Admin Dashboard
     *
     */
    public function testAdminPage()
    {
        $this->checkAdminPrivilege('/admin');
    }

    /**
     * checks if Login to admin is accessible (must be accessible without being logged in!)
     * and redirect if already logged in
     */
    public function testAdminLoginPage()
    {
        $this->checkResponse('/admin/login', $this->SUCCESS);
        $this->checkResponse('/admin/login', $this->SUCCESS, $this->bad_user);
        $this->checkResponse('/admin/login', $this->SUCCESS, $this->valid_user);
        $this->checkResponse('/admin/login', $this->REDIRECT, $this->valid_user, $this->ADMINDRIVER);
    }


    /**
     * Test if logout is reachable and successful and it can be logged in again afterwards
     *
     */
    public function testAdminLogoutPage()
    {
        $this->markTestSkipped('does not work as intended yet');
        $this->be($this->valid_user, 'admin');
        $response = $this->post('/admin/logout');
        $response->assertStatus(302); //should be redirected to login but doesn't work uwu
        $this->assertFalse($this->isAuthenticated());
    }


    /**
     * Test if admin password email works
     */
    public function testAdminPasswordEmailPage()
    {
        $this->markTestSkipped('not yet implemented');
        $response = $this->get('/admin/password/email');
        $response->assertStatus(200);
    }

    /**
     * Test if Admin can reset password
     */
    public function testAdminPasswordResetPage()
    {
        $this->markTestSkipped('not yet implemented');
        $response = $this->get('/admin/password/reset');
        $response->assertStatus(200);
    }

    /**
     * Test if admin page for pages is reachable and works
     */
    public function testAdminPagesPage()
    {
        $this->checkAdminPrivilege('/admin/seiten');
    }

    /**
     * Test if create page is reachable and works
     */
    public function testAdminCreatePagePage()
    {
        //TODO check if it is created correctly
        $this->checkAdminPrivilege('/admin/seiten/erstellen');
    }

    /**
     * Test if mediathek is accessible and works
     */
    public function testAdminMediathekPage()
    {
        $this->checkAdminPrivilege('/admin/mediathek');
    }

    /**
     * Test if "Lebensbereiche" is accessible and works
     */
    public function testAdminLifeAreasPage()
    {
        $this->checkAdminPrivilege('/admin/lebensbereiche');
    }

    /**
     * Test if "EditLebenbereich" is accessible
     */
    public function testAdminEditLifeAreas()
    {
        //TODO dynamically test for all entries
        $this->checkAdminPrivilege('/admin/lebensbereiche/1/bearbeiten');
        $this->markTestIncomplete('do not know how to iterate throw all the lebensbereiche');
    }

    /**
     * Test if "Kompetenzstufen" works and is accessible
     */
    public function testAdminExpertiseLevelPage()
    {
        $this->markTestSkipped('not yet implemented');
        $this->checkAdminPrivilege('/admin/kompetenzstufen');
    }

    /**
     * Test if Snippets works and is accessible
     */
    public function testAdminSnippetsPage()
    {
        $this->checkAdminPrivilege('/admin/snippets');
    }

    /**
     * Test if create snippet works and is accessible
     */
    public function testAdminCreateSnippetsPage()
    {
        $this->checkAdminPrivilege('/admin/snippets/erstellen');
        $this->markTestIncomplete('we don\'t yet check if saving snippets works!');
    }

    /**
     * Test if administration page works and is accessible
     */
    public function testAdminUserAdministrationPage()
    {
        $this->markTestSkipped('not yet implemented');
        $this->checkAdminPrivilege('/admin/benutzerverwaltung');
    }

    /**
     * Test if admin can create a user and page is accessible
     */
    public function testAdminCreateUserAdministrationPage()
    {
        $this->markTestSkipped('not yet implemented');
        $this->checkAdminPrivilege('/admin/benutzerverwaltung/erstellen');
    }

    /**
     * Test if admin profile page is working and accessible
     */
    public function testAdminProfilPage()
    {
        $this->checkAdminPrivilege('/admin/profil');
    }

    /**
     * Test if learning statistics page works and is accessible
     */
    public function testAdminLearnstatistikPage()
    {
        $this->checkAdminPrivilege('/admin/lernstatistik');
    }
}
