<?php

namespace Tests\Feature;

use App\Admin;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class APITest extends TestCase
{
    private $valid_user;
    private $bad_user;
    private $ADMINDRIVER = 'admin'; //Driver) that has admin privileges
    private $REDIRECT = 302; //HTTP codes
    private $SUCCESS = 200;

    public function SetUp(){
        parent::SetUp();
        $this->valid_user = factory(Admin::class)->create();
        $this->bad_user = factory(User::class)->create();
    }

    /**
     * Helper function for checking if a link returns the expected status (optionally with logged in user and appropriate guard as driver or not)
     * Logs out again if a user is supplied.
     * @param string $uri
     * @param int $expectStatus
     * @param Model $asUser
     * @param String $driver
     */
    public function checkResponse(string $uri, int $expectStatus, Model $asUser = null, String $driver = '')
    {
        /** @var Boolean $shouldLogout used to check if we have to log out again at the end.*/
        $shouldLogout = false;
        if (! is_null($asUser))
        {
            $shouldLogout = true;
            if (str_contains($driver, 'admin'))
            {
                $this->be($asUser, $driver);
            }
            else
            {
                $this->be($asUser);
            }
        }
        $this->get($uri)->assertStatus($expectStatus);
        if($shouldLogout)
        {
            Auth::logout();
        }

    }

    /**
     * Checks if a page given by uri is accessible, and only accessible, by an authenticated admin and redirects otherwise
     * @param string $uri
     */
    public function checkAdminPrivilege(string $uri)
    {
        $this->checkResponse($uri, $this->REDIRECT);
        $this->checkResponse($uri, $this->REDIRECT, $this->bad_user);
        $this->checkResponse($uri, $this->SUCCESS, $this->valid_user, $this->ADMINDRIVER);
    }

    public function testAdminDashboardAPI()
    {
        $this->checkAdminPrivilege('/api/v1/admindashboard');
    }

    public function testPagesOverviewAPI()
    {
        $this->checkAdminPrivilege('/api/v1/pagesoverview');
    }

    public function testSnippetOverviewAPI()
    {
        $this->checkAdminPrivilege('/api/v1/snippetsoverview');
    }

    public function testAreaOfInterestOverview()
    {
        $this->checkAdminPrivilege('/api/v1/areaofinterestoverview');
    }

    public function testAdminSkillLevel()
    {
        $this->checkAdminPrivilege('/api/v1/adminskilllevel');
    }
}
