<?php
/**
 * Created by PhpStorm.
 * User: Königin
 * Date: 24/12/2017
 * Time: 22:19
 */

namespace Tests\Feature;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HttpUserPageTest extends TestCase
{
    private $valid_user;
    private $REDIRECT = 302; //HTTP codes
    private $SUCCESS = 200;
    //private $bad_user; //needed later to simulate a "wrong" user

    public function SetUp(){
        parent::SetUp();
        $this->valid_user = factory(User::class)->create();
        //$this->bad_user = factory(User::class)->make(); //should not be in database
    }

    /**
     * Checks to see if given uri can be accessed with the specified user, and if it returns the appropriate response.
     * If $asUser is provided, will log out again after.
     * If both optional options are left empty, it is equivalent to "$this->get($uri)->assertSuccess();"
     * @param string $uri
     * @param int $expectedStatus
     * @param Model|null $asUser
     */
    public function checkResponse(string $uri, int $expectStatus = 200, Model $asUser = null)
    {
        /** @var Boolean $shouldLogout used to check if we have to log out again at the end. */
        $shouldLogout = false;
        if (!is_null($asUser)) {
            $shouldLogout = true;
            $this->be($asUser);
        }
        $this->get($uri)->assertStatus($expectStatus);
        if($shouldLogout)
        {
            Auth::logout();
        }
    }

    /**
     * Checks if a page given by uri is accessible, and only accessible, by an authenticated user and redirects otherwise
     * @param string $uri
     */
    public function checkUserPrivilege(string $uri)
    {
        $this->checkResponse($uri, 302);
        $this->checkResponse($uri, 200, $this->valid_user);
    }

    /**
     * Test if user dashboard (lesen) is accessible by user only
     */
    public function testUserPage()
    {
        $this->checkUserPrivilege('/lesen');
    }

    /**
     * Test if the "Lebensbereiche" can be accessed by user only.
     */
    public function testUserLebensbereiche()
    {
        $this->checkUserPrivilege('/lesen/1');
        $this->checkUserPrivilege('/lesen/2');
        $this->checkUserPrivilege('/lesen/3');
        $this->checkResponse('/lesen/100', 404, $this->valid_user); //test with inexistent values too!
        $this->checkResponse('/lesen/100', 302); //should not allow non logged in users to snoop possible viable pages
        $this->markTestIncomplete('should iterate through all possible "lebensbereiche"');
    }

    /**
     * Test if the Login page is accessible
     */
    public function testUserLoginPage()
    {
        $response = $this->get('/login');
        $response->assertStatus(200);
    }

    /**
     * Test if the registration page is accessible
     */
    public function testUserRegistrationPage()
    {
        $response = $this->get('/register');
        $response->assertStatus(200);
    }

    /**
     * test if the password reset page is accessible
     */
    public function testUserPasswordResetPage()
    {
        $response = $this->get('password/reset');
        $response->assertStatus(200);
    }

    /**
     * Test if learning statistic page works and is accessible
     */
     public function testLearningStatisticPage()
    {
        $this->markTestSkipped('not yet implemented');
        $this->checkUserPrivilege('/lesen/lernstatistik');
    }

    /**
     * Test if setting page works and is accessible
     */
    public function testSettingPage()
    {
        $this->markTestSkipped('not yet implemented');
        $this->checkUserPrivilege('/lesen/einstellungen');
    }

}

