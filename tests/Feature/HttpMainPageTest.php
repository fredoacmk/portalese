<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HttpMainPageTest extends TestCase
{
    /**
     * Test if we can call the start page
     */
    public function testStartPage()
    {
        $url = getenv('APP_URL');
        $response = $this->get($url);

        $response->assertStatus(200);
        //$response->assertHeader(http_head());
    }

    /**
     * Test if we can open the "contact" page
     */
    public function testContactPage()
    {
        $response = $this->get('/kontakt');

        $response->assertStatus(200);
    }

    /**
     * Test if we can open the "about us" page
     */
    public function testAboutUsPage()
    {
        $response = $this->get('/über-uns');

        $response->assertStatus(200);
    }

     /**
      * Tests to see if we can open the user and admin respective pages are in their respective test cases.
      * Are tested in admin page and main page respectively.
      */
}
