<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class UserLoginTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testLogin()
    {

        $userA = User::find(1);
        $url = getenv('APP_URL');


        $this->browse(function ($browser) use ($userA, $url) {
            $browser->visit($url . 'login')
                ->type('email', $userA->email)
                ->type('password', 'Testing12345')
                ->press('Login')
                ->assertPathIs('/lesen')
                ->logout();
        });
    }

    public function testLoginFails() {
        $userB = User::find(1);
        $url = getenv('APP_URL');

        $this->browse(function ($browser) use ($userB, $url) {
            $browser->visit($url . 'login')
                ->type('email', $userB->email)
                ->type('password', 'someWrongPassword')
                ->press('Login')
                ->assertPathIs('/login');
        });
    }
}
