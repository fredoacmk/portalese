<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ExampleTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $url = getenv('APP_URL');

        $this->browse(function (Browser $browser) use ($url) {
            $browser->visit($url)
                ->assertSee('Laravel');
        });
    }
}
