<?php

namespace Tests\Browser;

use App\Admin;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CreatePageTest extends DuskTestCase
{
    public function testLogin()
    {

        $userA = Admin::find(1);
        $url = getenv('APP_URL');


        $this->browse(function ($browser) use ($userA, $url) {
            $browser->visit($url . 'admin/login')
                ->type('email', $userA->email)
                ->type('password', 'Testing12345')
                ->press('Login')
                ->assertPathIs('/admin');
        });
    }
    /**
     * Tests if clicking Cancel redirects to the page overview.
     *
     * @return void
     */
    public function testClickCancel()
    {
        $admin = Admin::find(1);
        $url = getenv('APP_URL');

        $this->browse(function ($browser) use ($admin, $url) {
           $browser->visit($url . 'admin/login')
               ->loginAs($admin)
               ->visit($url . 'admin/seiten/erstellen' )
               ->press('Abbrechen')
               ->assertPathIs('/admin/seiten/erstellen') //Todo, make rederection to /seiten work. Remove /erstellen from this path.
               ->logout();
        });
    }

    public function testSetErrorClass() {
        $admin = Admin::find(1);
        $url = getenv('APP_URL');
        //$text1 = "Bitte füllen Sie dieses Feld aus";  //Todo, how to assertSee a tooltip?
        $text1 = "Lebensbereiche"; // Only for testing...
        $text2 = "Bitte wählen Sie einen Eintrag aus der Liste";

        $this->browse(function ($browser) use ($admin, $url, $text1, $text2) {
            $browser->visit($url . 'admin/login')
                ->loginAs($admin)
                ->visit($url . 'admin/seiten/erstellen' )
                ->press('Speichern')
                ->assertSee($text1)
                ->assertDontSee($text2)
                ->logout();
        });
    }

}
