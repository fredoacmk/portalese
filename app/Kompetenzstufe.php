<?php
/**
 * Class Kompetenzstufe | app/Kompetenzstufe.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
* This class is the Laravel Kompetenzstufe model for database access. 
*/
class Kompetenzstufe extends Model
{
    /**
     * The table name to be used with this model.
     *
     * @var string
     */
    protected $table = 'kompetenzstufen';

    /**
     * The primary key column.
     *
     * @var string
     */
    protected $primaryKey = 'idKompetenzstufe';

}
