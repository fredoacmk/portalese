<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrueFalseFragen extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tffragens';

    /**
     * The primary key column.
     *
     * @var string
     */
    protected $primaryKey = 'idTFFrage';
}
