<?php
/**
 * Class ResultController | app/Http/Controllers/ResultController.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\LPDataService;

/**
* This class is the controller for results.
*/
class ResultController extends Controller
{

    /** @var LPDataService $LPDataService Class member for LPDataService. */
    private $LPDataService;

    /**
     * Create a new controller instance.
     * 
     * @param LPDataService $LPDataService Instance of LPDataService.
     * @return void
     * 
     */
    public function __construct(LPDataService $LPDataService)
    {
        $this->middleware('auth');
        $this->LPDataService = $LPDataService;
    }

    /**
     * Evaluate results of solved exercise.
     *
     * @param Request
     */
    public function evaluate(Request $request)
    {
        $answers = $request->toArray();
        $snippets = $this->LPDataService::getSnippetsBySeiteId($request->idSeite);

        $onlyTF = array();
        $onlyMC = array();
        
        foreach ($answers as $key => $value) 
        {
            if (strpos($key, 'MC') === 0) 
            {
                $onlyMC[$key] = $value;
            }
            if (strpos($key, 'TF') === 0) 
            {
                $onlyTF[$key] = $value;
            }
        }

        $combined = [];
        $combined[] = $this->evaluateMC($snippets[0], $onlyMC);
        $combined[] = $this->evaluateTF($snippets[1], $onlyTF);
        $combined = json_encode($combined);

        return view('leseportal-user.user-results', compact('combined'));

    }

    /**
     * Evaluate MC.
     *
     * @param array Array of MC snippets, 
     * @param array Array of solved MC.
     * @return array Mutated array.
     */
    private function evaluateMC($arr_snippet, $arr_solved)
    {
        if(count($arr_solved) === 0) return [];

        unset($arr_snippet['type']);

        foreach ($arr_solved as $solvedKey => $solvedValue)
        {
            $exploded = explode("_", $solvedKey);
            $idMC = $exploded[1];
            $idMC = (int)$idMC;
            $rowQ = $exploded[2];

            foreach ($arr_snippet as $snippetKey => $snippetValue) 
            {
                if ($snippetValue->id == $idMC)
                {
                    $concatedKey = "richtig" . $rowQ;

                    if($snippetValue->$concatedKey === 1) 
                    {
                        $snippetValue->$concatedKey = true;
                    } else {
                        $snippetValue->$concatedKey = false;
                    }
                    
                }

            }
        }   
        return $arr_snippet;
    }

    /**
     * Evaluate TF.
     *
     * @param array Array of TF snippets, 
     * @param array Array of solved TF.
     */
    private function evaluateTF($arr_snippet, $arr_solved)
    {
        if(count($arr_solved) === 0) return [];

        unset($arr_snippet['type']);

        foreach ($arr_solved as $solvedKey => $solvedValue)
        {
            $exploded = explode("_", $solvedKey);
            $idTF = $exploded[1];
            $idTF = (int)$idTF;
            $solveString = str_replace('selected ', '', $solvedValue);
            $solve = $solveString === 'true' ? 1 : 0;

            foreach ($arr_snippet as $snippetKey => $snippetValue) 
            {
                if (!property_exists($snippetValue, 'correct')) 
                {
                    $snippetValue->correct = false;
                }

                if ($snippetValue->id === $idTF && $snippetValue->richtig === $solve)
                {
                    $snippetValue->correct = true;
                }
            }
        }

        return $arr_snippet;
    }
}
