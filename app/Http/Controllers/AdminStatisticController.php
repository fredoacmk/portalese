<?php
/**
 * Class AdminStatisticController | app/Http/Controllers/AdminStatisticController.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
* This class is the controller for admin statistics.
*/
class AdminStatisticController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('leseportal-admin.admin-learning-statistic', compact('lernstatistik'));
    }
}
