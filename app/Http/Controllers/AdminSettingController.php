<?php
/**
 * Class AdminSettingController | app/Http/Controllers/AdminSettingController.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App\Http\Controllers;

use App\AdminSetting;
use Illuminate\Http\Request;

/**
* This class is the controller for admin settings.
*/
class AdminSettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('leseportal-admin.admin-setting', compact('einstellungen'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request
     * @param  AdminSetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdminSetting $adminsetting)
    {
        //
    }


}
