<?php
/**
 * Class AdminKompetenzstufeController | app/Http/Controllers/AdminKompetenzstufeController.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App\Http\Controllers;

use App\Kompetenzstufe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

/**
* This class is the controller for user competence levels.
*/
class AdminKompetenzstufeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show all Kompetenzstufe in a list.
     *
     * @return void
     */
    public function index()
    {
        $kompetenzstufe = Kompetenzstufe::all()->toArray();

        return view('leseportal-admin.admin-skill-level');
    }

    /**
     * Create skill level
     *
     * @return void
     */
    public function create()
    {
        return view('leseportal-admin.admin-create-skill-level');
    }

    /**
     * Store a new Kompetenzstufe into the DB.
     *
     * @return void
     */
    public function store(Request $request)
    {
        $competence = new Kompetenzstufe();

        $competence->level = $request->input('title');
        $competence->beschreibung = $request->input('content');

        $competence->save();

        return Redirect::route('admin.kompetenzstufe')->with('success','Kompetenzstufe erfolgreich gespeichert!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Seite  $kompetenzstufe
     * @return \Illuminate\Http\Response
     */

    public function edit(Kompetenzstufe $kompetenzstufe)
    {
        return view('leseportal-admin.admin-edit-skill-level', compact ('kompetenzstufe'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Seite  $kompetenzstufe
     * @return \Illuminate\Http\Response
    */
    public function show(Kompetenzstufe $kompetenzstufe)
    {
        return view('leseportal-admin.admin-edit-skill-level', compact ('kompetenzstufe'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kompetenzstufe  $kompetenzstufe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idKompetenzstufe)
    {
        $kompetenzstufe = Kompetenzstufe::find($idKompetenzstufe);

        $kompetenzstufe->level = $request->input('title');
        $kompetenzstufe->beschreibung = $request->input('content');

        $kompetenzstufe->save();

        return Redirect::route('admin.kompetenzstufe')->with('success','Kompetenzstufe aktualisiert!');
    }

    /**
     * Delete the specific Skill Level.
     *
     * @param Kompetenzstufe
     */
    public function destroy($kompetenzstufe)
    {
        if(is_numeric($kompetenzstufe))
        {
            Kompetenzstufe::destroy($kompetenzstufe);
            return 200;
        } else
        {
            return 404;
        }
    }
}
