<?php
/**
 * Class UserSeitenController | app/Http/Controllers/UserSeitenController.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App\Http\Controllers;

use App\Kompetenzstufe;
use App\Lebensbereich;
use App\Seite;
use Illuminate\Http\Request;

/**
* This class is the controller for user views.
*/
class UserSeitenController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show Seiten with that Lebensbereich.
     *
     * @param Lebensbereich
     */
    public function index(Lebensbereich $lebensbereich)
    {
        $seiten = Seite::all()->toArray();
        $seitenModified = array_map(function($seite){

            $seite['idLebensbereich'] = Lebensbereich::find($seite['idLebensbereich'])->name;
            $seite['idKompetenzstufe'] = Kompetenzstufe::find($seite['idKompetenzstufe'])->level;

            return $seite;
        }, $seiten);

        $kompetenzstufen = Kompetenzstufe::all();
        $lebensbereich = Lebensbereich::all();
        return view('leseportal-user.user-area-of-interest', compact('seitenModified', 'kompetenzstufen', 'lebensbereich'));
    }

    /**
     * Show Seiten with that Lebensbereich.
     *
     * @param Lebensbereich
     * @param Seite
     */
    public function read(Lebensbereich $lebensbereich, Seite $seite)
    {
        return view('leseportal-user.user-page-show', compact('seite', 'lebensbereich'));
    }

    /**
     * Show Exercise page for specific Seite.
     *
     * @param Lebensbereich
     * @param Seite
     * @param Snippet
     */
    public function solve(Lebensbereich $lebensbereich, Seite $seite, Snippet $snippet)
    {

    }


}
