<?php
/**
 * Class UserStatisticController | app/Http/Controllers/UserStatisticController.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
* This class is the controller for user statistics.
*/
class UserStatisticController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('leseportal-user.user-learning-statistic', ['name' => 'lulu']);
    }
}
