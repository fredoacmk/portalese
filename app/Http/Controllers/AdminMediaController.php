<?php
/**
 * Class AdminMediaController | app/Http/Controllers/AdminMediaController.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
* This class is the controller for admin meadia.
*/
class AdminMediaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show all media in a list.
     *
     * @return void
     */
    public function index()
    {
        return view('leseportal-admin.admin-media-center', compact('mediathek'));
    }
}
