<?php
/**
 * Class AdminSeitenController | app/Http/Controllers/AdminSeitenController.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App\Http\Controllers;

use App\Kompetenzstufe;
use App\Lebensbereich;
use App\Seite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Services\LPConverterService;


/**
* This class is the controller for admin pages.
*/
class AdminSeitenController extends Controller
{
    /** @var LPConverterService $LPConverterService Class member for LPConverterService. */
    private $LPConverterService;

    /**
     * Create a new controller instance.
     *
     * @param LPConverterService $LPConverterService Instance of LPConverterService.
     * @return void
     */
    public function __construct(LPConverterService $LPConverterService)
    {
        $this->middleware('auth:admin');
        $this->LPConverterService = $LPConverterService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $seiten = Seite::all()->toArray();

        $seitenModified = array_map(function($seite){

            $seite['idLebensbereich'] = Lebensbereich::find($seite['idLebensbereich'])->name;
            $seite['idKompetenzstufe'] = Kompetenzstufe::find($seite['idKompetenzstufe'])->level;

            return $seite;
        }, $seiten);

        return view('leseportal-admin.admin-pages', compact('seitenModified'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lebensbereiche = Lebensbereich::all();
        $kompetenzstufen = Kompetenzstufe::all();

        return view('leseportal-admin.admin-create-page', compact('lebensbereiche', 'kompetenzstufen'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $seite = new Seite;

        $interest = $request->input('interest');
        $competence = $request->input('competence');

        $seite->titel = $request->input('title');
        $seite->inhalt = $request->input('content');
        $seite->idLebensbereich = $interest;
        $seite->idKompetenzstufe = $competence;

        $seite->save();

        return Redirect::route('admin.seiten')->with('success','Seite erfolgreich gespeichert!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Seite  $seite
     * @return \Illuminate\Http\Response
     */
    public function show(Seite $seite)
    {
        return view('leseportal-admin.admin-page-show', compact('seite'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Seite  $seite
     * @return \Illuminate\Http\Response
     */
    public function edit(Seite $seite)
    {
        $seiten = Seite::where('idSeite', $seite->idSeite)->get()->toArray();

        $seitenModified = array_map(function($seite){

            $seite['lebensbereich'] = $this->LPConverterService->convertAreOfInterest($seite['idLebensbereich']);
            $seite['kompetenzstufe'] = $this->LPConverterService->convertCompetence($seite['idKompetenzstufe']);

            return $seite;
        }, $seiten);

        return view('leseportal-admin.admin-edit-page', compact ('seitenModified'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Seite  $seite
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idSeite)
    {
        $seite = Seite::find($idSeite);

        $interest = $request->input('interest');
        $competence = $request->input('competence');

        $seite->titel = $request->input('title');
        $seite->inhalt = $request->input('content');
        $seite->idLebensbereich = $interest;
        $seite->idKompetenzstufe = $competence;

        $seite->save();

        return Redirect::route('admin.seiten')->with('success','Seite aktualisiert!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Seite  $seite
     * @return \Illuminate\Http\Response
     */
    public function destroy($idSeite)
    {
        if(is_numeric($idSeite))
        {
            Seite::destroy($idSeite);
            return 200;
        } else
        {
            return 404;
        }
    }
}
