<?php
/**
 * Class AdminDashBoardController | app/Http/Controllers/AdminDashBoardController.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App\Http\Controllers;

use App\Kompetenzstufe;
use App\Lebensbereich;
use App\Seite;
use App\Snippet;
use App\User;
use App\Admin;
use Illuminate\Http\Request;

/**
* This class is the controller for admin dashboard view.
*/
class AdminDashBoardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard (with count of Admins, Seiten, Lebensbereiche and Users).
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('leseportal-admin.admin-dashboard');
    }
}
