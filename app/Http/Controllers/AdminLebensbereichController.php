<?php
/**
 * Class AdminLebensbereichController | app/Http/Controllers/AdminLebensbereichController.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App\Http\Controllers;

use App\Lebensbereich;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

/**
* This class is the controller for admin area of interest.
*/
class AdminLebensbereichController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the Interests.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('leseportal-admin.admin-area-of-interest');
    }

    /**
     * Create Interest
     *
     * @return void
     */
    public function create()
    {
        return view('leseportal-admin.admin-create-area-of-interest');
    }

    /**
     * Store a Lebensbereich into the DB.
     *
     * @return void
     */
    public function store(Request $request)
    {
        $interest = new Lebensbereich();

        $interest->name = $request->input('title');
        $interest->beschreibung = $request->input('content');

        $interest->save();

        return Redirect::route('admin.lebensbereiche')->with('success','Lebensbereich erfolgreich gespeichert!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Seite  $lebensbereich
     * @return \Illuminate\Http\Response
     */
    public function edit(Lebensbereich $lebensbereich)
    {
        return view('leseportal-admin.admin-edit-area-of-interest', compact ('lebensbereich'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lebensbereich  $lebensbereich
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idLebensbereich)
    {
        $lebensbereich = Lebensbereich::find($idLebensbereich);

        $lebensbereich->name = $request->input('title');
        $lebensbereich->beschreibung = $request->input('content');

        $lebensbereich->save();

        return Redirect::route('admin.lebensbereiche')->with('success','Lebensbereich aktualisiert!');
    }

    /**
     * Delete the specific Lebensbereich.
     *
     * @param Lebensbereich
     */
    public function destroy($lebensbereich)
    {
        if(is_numeric($lebensbereich))
        {
            Lebensbereich::destroy($lebensbereich);
            return 200;
        } else
        {
            return 404;
        }
    }
}