<?php
/**
 * Class UserDashboardController | app/Http/Controllers/UserDashboardController.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App\Http\Controllers;

use App\Kompetenzstufe;
use App\Lebensbereich;
use App\Seite;
use Illuminate\Http\Request;

/**
* This class is the controller for user dashboard.
*/
class UserDashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $seiten = Seite::all()->toArray();

        $seitenModified = array_map(function($seite){

            $seite['idLebensbereich'] = Lebensbereich::find($seite['idLebensbereich'])->name;
            $seite['idKompetenzstufe'] = Kompetenzstufe::find($seite['idKompetenzstufe'])->level;

            return $seite;
        }, $seiten);

        return view('leseportal-user.user-dashboard', compact('seitenModified'));
    }
}
