<?php
/**
 * Class ApiAdminController | app/Http/Controllers/api/ApiAdminController.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App\Http\Controllers\api;

use App\Admin;
use App\DragAndDrop;
use App\MultipleChoiceFragen;
use App\TrueFalse;
use App\User;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Seite;
use App\Lebensbereich;
use App\Kompetenzstufe;
use App\Snippet;
use App\Services\LPConverterService;
use Illuminate\Support\Facades\DB;
use App\SeiteToSnippet;

/**
* This class is the controller for admin api requests.
*/
class ApiAdminController extends Controller
{
    /** @var LPConverterService $LPConverterService Class member for LPConverterService. */
    private $LPConverterService;

    /**
     * Create a new controller instance.
     *
     */
    public function __construct(LPConverterService $LPConverterService)
    {
        $this->middleware('auth:admin');
        $this->LPConverterService = $LPConverterService;
    }

    /**
     * Show a table with a list of all sites.
     *
     * @return JSON for all sites.
     */
    public function getPagesOverview()
    {
        $seiten = Seite::select('idSeite','titel', 'idLebensbereich', 'idKompetenzstufe', 'updated_at')
            ->get()
            ->toArray();
        
        $seitenModified = array_map(function($seite){

            $seite['lebensbereich'] = $this->LPConverterService->convertAreOfInterest($seite['idLebensbereich']);
            $seite['kompetenzstufe'] = $this->LPConverterService->convertCompetence($seite['idKompetenzstufe']);

            return $seite;
        }, $seiten);

        return $seitenModified;
    }

    /**
     * Show the application dashboard (with count of Seiten, Lebensbereiche, Snippets, Users, Admins).
     *
     * @return array for all counts.
     */

    public function getDashboardCounts()
    {
        $DashboardCounts = array(
            "siteCount"    => Seite::count(),
            "lebensbereicheCount"  => Lebensbereich::count(),
            "snippetsCount"  => MultipleChoiceFragen::count()+ TrueFalse::count()+DragAndDrop::count(),
            "userCount" => User::count(),
            "adminCount" => Admin::count()
        );
        return json_encode($DashboardCounts);
    }

    /**
     * Provide data for admin snippet overview.
     *
     * @return JSON for all pages.
     */
    public function getSnippetsOverview()
    {
        // Empty array for later.
        $allSnippets = Array();

        // Get all MLTPCH.
        $mc = MultipleChoiceFragen::all()->toArray();

        // Map some elements to text.
        $mc = array_map(function($snippet) {
            $snippet['text_title'] = Seite::find($snippet['idSeite'])->titel;
            $snippet['snippet_type'] = $this->LPConverterService->convertSnippetTypeToName(Snippet::find($snippet['snippetTypeId'])->type);
            $snippet['snippet_idSnippet'] = $snippet['id'];
            return $snippet;
        }, $mc);

        // Get all True/False.
        $tf = TrueFalse::all()->toArray();

        // Map some elements to text.
        $tf = array_map(function($snippet) {
            $snippet['text_title'] = Seite::find($snippet['idSeite'])->titel;
            $snippet['snippet_type'] = $this->LPConverterService->convertSnippetTypeToName(Snippet::find($snippet['snippetTypeId'])->type);
            $snippet['snippet_idSnippet'] = $snippet['id'];
            return $snippet;
        }, $tf);
        
        // Get all Drag and Drop.
        $dd = DragAndDrop::all()->toArray();

        // Map some elements to text.
        $dd = array_map(function($snippet) {
            $snippet['text_title'] = Seite::find($snippet['idSeite'])->titel;
            $snippet['snippet_type'] = $this->LPConverterService->convertSnippetTypeToName(Snippet::find($snippet['snippetTypeId'])->type);
            $snippet['snippet_idSnippet'] = $snippet['id'];
            return $snippet;
        }, $dd);

        // Add all partial arrays to allSnippets
        $allSnippets[] = $mc;
        $allSnippets[] = $tf;
        $allSnippets[] = $dd;

        return $allSnippets;

    }

    /**
     * Provide data for Admin Area of Interest
     *
     * @return JSON for all pages.
     */
    public function getAreaOfInterestOverview()
    {
        $areaOfInterest = Lebensbereich::all()->toJson();

        return $areaOfInterest;
    }

    /**
     * Provide data for Admin Area of Interest
     *
     * @return JSON for all pages.
     */
    public function editAreaOfInterest($idLebensbereich)
    {
        $lebensbereich = Lebensbereich::find($idLebensbereich);
        return json_encode($lebensbereich);
    }

    /**
     * Provide data for Admin competence levels
     *
     * @return JSON for all pages.
     */
    public function getSkillLevels()
    {
        $skillLevel = Kompetenzstufe::all()->toJson();

        return $skillLevel;
    }
}
