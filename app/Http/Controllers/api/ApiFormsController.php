<?php
/**
 * Class ApiFormsController | app/Http/Controllers/api/ApiFormsController.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

use App\Lebensbereich;
use App\Kompetenzstufe;
use App\Seite;
/**
* This class is the controller for form/list data requests.
*/
class ApiFormsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return The specific middleware if user is logged in. Otherwise 401 - not authorized.
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {

            if(Auth::check()) {

                $this->middleware('auth');
                return $next($request);

            } elseif (Auth::guard('admin')->check()) {

                $this->middleware('auth:admin');
                return $next($request);

            }

            abort(401);

        });
    }

    /**
     * Returns the list of Interests as JSON.
     *
     * @return Interests JSON.
     */
    public function getInterestList()
    {
        $interests = Lebensbereich::all()->toJson();
        return $interests;
    }

    /**
     * Returns the list of Competence as JSON.
     *
     * @return Competence JSON.
     */
    public function getCompetenceList()
    {
        $competences = Kompetenzstufe::all()->toJson();
        return $competences;
    }

    /**
     * Returns the list of available snippets as JSON.
     *
     * @return Snippets JSON.
     */
    public function getSnippetDropdown()
    {
        $list = array(
            'MC' => __('admin.MC'), 
            'DD' => __('admin.DD'), 
            'TF' => __('admin.TF'), 
            'LT' => __('admin.LT')
        );
        return json_encode($list);
    }


}
