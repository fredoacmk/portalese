<?php
/**
 * Class ApiUserController | app/Http/Controllers/api/ApiUserController.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App\Http\Controllers\api;

use App\Lebensbereich;
use App\SeiteToSnippet;
use App\Snippet;
use App\TrueFalseFragen;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Seite;
use Illuminate\Support\Facades\DB;
use App\Services\LPConverterService;
use App\Services\LPDataService;

/**
* This class is the controller for user api requests.
*/
class ApiUserController extends Controller
{
    /** @var LPConverterService $LPConverterService Class member for LPConverterService. */
    private $LPConverterService;

    /** @var LPDataService $LPDataService Class member for LPDataService. */
    private $LPDataService;

    /**
     * Create a new controller instance.
     *
     */
    public function __construct(LPConverterService $LPConverterService, LPDataService $LPDataService)
    {
        $this->middleware('auth');
        $this->LPConverterService = $LPConverterService;
        $this->LPDataService = $LPDataService;
    }

    /**
     * Returns the list of last 10 current texts as JSON ordered by created_at column.
     *
     * @return Texts JSON.
     */
    public function getDashboard() 
    {
        $seiten = Seite::select('idSeite','titel', 'idLebensbereich', 'idKompetenzstufe')
            ->get()
            ->toArray();
        
        $seitenModified = array_map(function($seite){

            $seite['lebensbereich'] = $this->LPConverterService->convertAreOfInterest($seite['idLebensbereich']);
            $seite['kompetenzstufe'] = $this->LPConverterService->convertCompetence($seite['idKompetenzstufe']);

            return $seite;
        }, $seiten);

        return json_encode($seitenModified);
    }

    /**
     * Returns all Snippets to a certain text as JSON.
     *
     * @return Snippet JSON.
     */
    public function getSnippets($seite_idSeite)
    {
        // OUTSOURCED TO SERVICE :-)
        // $temp[] = DB::table('mcfragens')
        //     ->where('idSeite', $seite_idSeite)
        //     ->get();
        // $temp[0]['type'] = "MC";

        // $temp[] = DB::table('trueandfalses')
        //     ->where('idSeite', $seite_idSeite)
        //     ->get();
        // $temp[1]['type'] = "TF";
        
        // if(count($temp[0]) === 1 && count($temp[1]) === 1) 
        // {
        //     $temp = [];
        // }

        $snippets = $this->LPDataService::getSnippetsBySeiteId($seite_idSeite);

        foreach ($snippets as $key => $value) {
            unset($value['type']);
            foreach ($value as $k => $v) 
            {
                foreach ($v as $s => $w) 
                {
                    if(substr($s, 0, 7 ) === "richtig")
                    {
                        unset($v->$s);
                    }
                }
            }
        }
        $snippets[0]['type'] = "MC";
        $snippets[1]['type'] = "TF";

        if(count($snippets[0]) === 1 && count($snippets[1]) === 1)
        {
            $snippets = [];
        } else if (count($snippets[0]) === 1) {
            $snippets[0] = [];
        } else if (count($snippets[1]) === 1) {
            $snippets[1] = [];
        }

        return $snippets;


//        /* returns table SeiteToSnippet as array */
//        $seiteToSnippet = SeiteToSnippet::select('seite_idSeite', 'snippet_idSnippet')
//            ->where('seite_idSeite', $seite_idSeite)
//            ->get()
//            ->toArray();
//
//        /* isolates SnippetID column from SeiteToSnippet */
//        $isolatedSnippetID = array_column($seiteToSnippet, 'snippet_idSnippet');
//
//        /* array for key/value-pairs */
//        $list = [
//            1 => "MC",
//            2 => "TF",
//            3 => "DD",
//            4 => "LT"
//        ];
//
//        /* maps each key(snippetID) to according value */
//        foreach($isolatedSnippetID as $key => $value){
//            if($value == 1){
//                $result[] = array($isolatedSnippetID[$key] => $list[1]);
//            }
//            if($value == 2){
//                $result[] = array($isolatedSnippetID[$key] => $list[2]);
//            }
//            if($value == 3){
//                $result[] = array($isolatedSnippetID[$key] => $list[3]);
//            }
//            if($value == 4){
//                printf("not implemented yet");
//            }
//        }
//
//        /* isolates type column from result array */
//        $isolatedType = array();
//        array_unshift($result, null);
//        foreach (call_user_func_array("array_map", $result) as $value ) {
//            $isolatedType[] = $value;
//        }
//
//        /* get called data as json */
//        foreach($result as $iterate => $value){
//            switch($iterate){
//                case ($list[1]):
//                    $temp[] = DB::table('mcfragens')
//                    ->where('idMultipleChoice', $seite_idSeite)
//                    ->get();
//                    $temp[0]['type'] = "MC";
//                case ($list[2]):
//                    $temp[] = DB::table('trueandfalses')
//                    ->get();
//                    $temp[1]['type'] = "TF";
//                case ($list[3]):
//                    //not done yet;
//            }
//        }
//        return $temp;
    }
}
