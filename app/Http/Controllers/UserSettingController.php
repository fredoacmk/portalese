<?php
/**
 * Class UserSettingController | app/Http/Controllers/UserSettingController.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App\Http\Controllers;

use App\UserSetting;
use Illuminate\Http\Request;

/**
* This class is the controller for user settings.
*/
class UserSettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('leseportal-user.user-setting', ['name' => 'lulu']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //
    }



}
