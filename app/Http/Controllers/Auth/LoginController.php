<?php
/**
 * Class LoginController | app/Http/Controllers/Auth/LoginController.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

/**
* This class is the controller for user login.
*/
class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/lesen';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['logout', 'userLogout']]);
    }

    /**
     * Return redirect to startpage after logout. 
     *
     */
    public function userLogout()
    {
        Auth::guard('web')->logout();
        return redirect('/');
    }
}
