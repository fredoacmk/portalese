<?php
/**
 * Class ForgotPasswordController | app/Http/Controllers/Auth/ForgotPasswordController.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

/**
* This class is the controller for user forgot password.
*/
class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
}
