<?php
/**
 * Class AdminSnippetController | app/Http/Controllers/AdminSnippetController.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App\Http\Controllers;

use App\MultipleChoice;
use App\MultipleChoiceFragen;
use App\Snippet;
use App\Kompetenzstufe;
use App\Lebensbereich;
use App\Seite;
use App\TrueFalse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Services\LPConverterService;
use Illuminate\Support\Facades\DB;

/**
* This class is the controller for admin snippets.
*/
class AdminSnippetController extends Controller
{
    /** @var LPConverterService $LPConverterService Class member for LPConverterService. */
    private $LPConverterService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(LPConverterService $LPConverterService)
    {
        $this->middleware('auth:admin');
        $this->LPConverterService = $LPConverterService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('leseportal-admin.admin-snippets', compact('snippets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $snippet = Snippet::all()->toArray();
        $seiten = Seite::all()->toArray();

        $seitenModified = array_map(function($seite){

            $seite['idLebensbereich'] = Lebensbereich::find($seite['idLebensbereich'])->name;
            $seite['idKompetenzstufe'] = Kompetenzstufe::find($seite['idKompetenzstufe'])->level;

            return $seite;
        }, $seiten);
        return view('leseportal-admin.admin-create-snippet', compact('snippet', 'seitenModified'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $snippettype = $request->input('snippettype');

        switch($snippettype){
            case ("MC"):
                $this->saveMC($input, $request);
                break;
            case ("TF"):
                $this->saveTF($input, $request);
                break;
            case ("DD"):
                break;
        }

        return Redirect::route('admin.snippets')->with('success','Snippet erfolgreich gespeichert!');
    }

    /**
     * get multiple choices for saving.
     *
     * @param  $input
     * @return saved MC
     */
    public function saveMC($input, $request){

        /* Slice away first three rows from array */
        $temp_sliced = array_slice($input, 3);

        /* Sort keys from request ascending */
        $this->sortKeysAsc($temp_sliced);

        /* isolate each Question with according answers and "right-box" */
        $i = 1;
        Loop:
        foreach ($temp_sliced as $key => $value) {
            if (strpos($key, 'q' . $i) !== false) {
                $arr[$i-1][$key] = $value;
            }
        }
        $i++;
        if ($i < count($temp_sliced)){
            goto Loop;
        }

        /****************/
        /* SAVE INTO DB */
        /****************/
        for($j = 0; $j < count($arr); $j++){
            /* empty array each loop and save copy*/
            unset($newArr);
            $newArr[] = $arr[$j];
            $arrCopy = $newArr;

            /* Map to Array as Key->Value - Pair */
            $mapAsKeyValue = array();
            array_unshift($arrCopy, null);
            foreach (call_user_func_array("array_map", $arrCopy) as $key => $value) {
                $mapAsKeyValue[$key] = $value;
            }

            $i = $j + 1;
            $counter = 0;
            while (count($mapAsKeyValue) > 0) {
                foreach ($mapAsKeyValue as $key => $value) {
                    /* save questions*/
                    if (utf8_decode(strlen($key)) < 3) {
                        $mc = new MultipleChoiceFragen;
                        $mc->idSeite = $request->input('idSeite');
                        $mc->frage = $request->input($key);
                        $mapAsKeyValue = array_slice($mapAsKeyValue, 1);
                    }

                    /* save answers */
                    if (utf8_decode(strlen($key)) > 4 && utf8_decode(strlen($key)) < 7) {
                        if ($key == 'q' . $i . '_a1') {
                            $mc->antwort1 = $request->input('q' . $i . '_a1');
                            $mapAsKeyValue = array_slice($mapAsKeyValue, 1);
                            $counter++;
                        }
                        if ($key == 'q' . $i . '_a2') {
                            $mc->antwort2 = $request->input('q' . $i . '_a2');
                            $mapAsKeyValue = array_slice($mapAsKeyValue, 1);
                            $counter++;
                        }
                        if ($key == 'q' . $i . '_a3') {
                            $mc->antwort3 = $request->input('q' . $i . '_a3');
                            $mapAsKeyValue = array_slice($mapAsKeyValue, 1);
                            $counter++;
                        }
                        if ($key == 'q' . $i . '_a4') {
                            $mc->antwort4 = $request->input('q' . $i . '_a4');
                            $mapAsKeyValue = array_slice($mapAsKeyValue, 1);
                            $counter++;
                        }
                        if ($key == 'q' . $i . '_a5') {
                            $mc->antwort5 = $request->input('q' . $i . '_a5');
                            $mapAsKeyValue = array_slice($mapAsKeyValue, 1);
                            $counter++;
                        }
                    }

                    /* save answers */
                        if ($key == 'q' . $i . '_a1_richtig') {
                            $mc->richtig1 = true;
                            $mapAsKeyValue = array_slice($mapAsKeyValue, 1);
                            $counter--;

                        } else {
                            if ($counter > 0) {
                                $mc->richtig1 = null;
                                $counter--;
                            }
                        }
                        if ($key == 'q' . $i . '_a2_richtig') {
                            $mc->richtig2 = true;
                            $mapAsKeyValue = array_slice($mapAsKeyValue, 1);
                            $counter--;
                        } else {
                            if ($counter > 0) {
                                $mc->richtig2 = null;
                                $counter--;
                            }
                        }
                        if ($key == 'q' . $i . '_a3_richtig') {
                            $mc->richtig3 = true;
                            $mapAsKeyValue = array_slice($mapAsKeyValue, 1);
                            $counter--;
                        } else {
                            if ($counter > 0) {
                                $mc->richtig3 = null;
                                $counter--;
                            }
                        }
                        if ($key == 'q' . $i . '_a4_richtig') {
                            $mc->richtig4 = true;
                            $mapAsKeyValue = array_slice($mapAsKeyValue, 1);
                            $counter--;
                        } else {
                            if ($counter > 0) {
                                $mc->richtig4 = null;
                                $counter--;
                            }
                        }
                        if ($key == 'q' . $i . '_a5_richtig') {
                            $mc->richtig5 = true;
                            $mapAsKeyValue = array_slice($mapAsKeyValue, 1);
                        } else {
                            if ($counter > 0) {
                                $mc->richtig5 = null;
                                $counter--;
                            }
                        }

                    /* save into db */
                    $mc->save();
                }
            }
        }
    }

    /**
     * Sort keys from $request in ascending order
     *
     * @param  \Illuminate\Http\Request  $arrNew
     * @return sorted $arrNew
     */
    function sortKeysAsc(&$arrNew) {
        uksort($arrNew, function($b, $a) {
            $lenA = strlen($a);
            $lenB = strlen($b);
            if ($lenA == $lenB) {
                // If equal length, sort again by ascending
                $arrOrig = array($a, $b);
                $arrSort = $arrOrig;
                rsort($arrSort);
                if ($arrOrig[0] !== $arrSort[0]) return 1;
            } else {
                // If not equal length, simple
                return $lenB - $lenA;
            }
        });
    }

    /**
     * get multiple choices for saving.
     *
     * @param  $input, $request
     * @return saved TF
     */
    public function saveTF($input, $request){
        /* Slice away first three rows from array */
        $temp_sliced = array_slice($input, 3);

        /* isolate each Question with according answers and "right-box" */
        $i = 1;
        Loop:
        foreach ($temp_sliced as $key => $value) {
            if (strpos($key, 'q' . $i) !== false) {
                $arr[$i-1][$key] = $value;
            }
        }
        $i++;
        if ($i < count($temp_sliced)){
            goto Loop;
        }


        /* save into DB */
        for($j = 0; $j < count($arr); $j++) {
            unset($newArr);
            $newArr[] = $arr[$j];
            $arrCopy = $newArr;

            /* Map to Array as Key->Value - Pair */
            $mapAsKeyValue = array();
            array_unshift($arrCopy, null);
            foreach (call_user_func_array("array_map", $arrCopy) as $key => $value) {
                $mapAsKeyValue[$key] = $value;
            }

            $i = $j+1;
            if(count($mapAsKeyValue) == 2){
                $tf = new TrueFalse();
                $tf->idSeite = $request->input('idSeite');
                $tf->statement = $request->input('q' . $i);
                $tf->richtig = true;
            } else {
                $tf = new TrueFalse();
                $tf->idSeite = $request->input('idSeite');
                $tf->statement = $request->input('q' . $i);
                $tf->richtig = false;
            }
            $tf->save();
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Snippet  $snippet
     * @return \Illuminate\Http\Response
     */
    public function show(Snippet $snippet)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Snippet  $snippet
     * @return \Illuminate\Http\Response
     */
    public function edit(Snippet $snippet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Snippet  $snippet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Snippet $snippet)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Snippet  $snippet
     * @return \Illuminate\Http\Response
     */
    public function destroy($type, $id)
    {
        $dbName = $this->LPConverterService->convertSnippetIdTypeToDbName($type);

        if(is_numeric($id)) 
        {
            DB::table($dbName)->where('id', $id)->delete();
            return 200;
        } else 
        {
            return 404;
        }
    }
}
