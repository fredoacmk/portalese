<?php
/**
 * Class GuestController | app/Http/Controllers/GuestController.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Redirect;

/**
* This class is the controller for guest.
*/
class GuestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(Auth::check()) {
            return Redirect::to('/lesen');
        } elseif (Auth::guard('admin')->check()) {
            return Redirect::to('/admin');
        }

            return view('home');
    }

    /**
     * Display contact view.
     *
     * @return \Illuminate\View\View
     */
    public function contact()
    {
        return view('contact');
    }

    /**
     * Display about view.
     *
     * @return \Illuminate\View\View
     */
    public function about()
    {
        return view('about');
    }



}
