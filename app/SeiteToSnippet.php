<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeiteToSnippet extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'seitetosnippets';

    /**
     * The primary key column.
     *
     * @var string
     */
    protected $primaryKey = 'seite_idSeite';
}
