<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DragAndDrop extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'draganddrops';

    /**
     * The primary key column.
     *
     * @var string
     */
    protected $primaryKey = 'id';
}
