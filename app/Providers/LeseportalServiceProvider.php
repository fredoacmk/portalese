<?php
/**
 * Class LeseportalServiceProvider | app/Providers/LeseportalServiceProvider.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\LPDataService;
use App\Services\LPConverterService;

/**
* This class is the provider for all Leseportal services.
*/
class LeseportalServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the Leseportal services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\LPDataService', function ($app) {
            return new LPDataService();
        });

        $this->app->bind('App\Services\LPConverterService', function ($app) {
            return new LPConverterService();
        });
    }
}
