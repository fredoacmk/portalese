<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MultipleChoiceFragen extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mcfragens';

    /**
     * The primary key column.
     *
     * @var string
     */
    protected $primaryKey = 'id';
}
