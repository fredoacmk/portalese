<?php
/**
 * Class UserStatistic | app/UserStatistic.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
* This class is the Laravel Kompetenzstufe model for database access. 
*/
class UserStatistic extends Model
{
    //
}
