<?php
/**
 * Class LPConverterService | app/Services/LPConverterService.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App\Services;

use App\Lebensbereich;
use App\Kompetenzstufe;
  
/**
* This class converts needed data for other php classes.
*/
class LPConverterService
{
    /**
     * Converts area of interest ID into the name of it.
     *
     * @param int 
     * @return string
     */
    public static function convertAreOfInterest($id)
    {
        return Lebensbereich::find($id)->name;
    }

    /**
     * Converts competence ID into the level of it.
     *
     * @param int 
     * @return string
     */
    public static function convertCompetence($id)
    {
        return Kompetenzstufe::find($id)->level;
    }


    /**
     * Converts Snippet type into DB name.
     *
     * @param string 
     * @return string
     */
    public static function convertSnippetTypeToDbName($typ)
    {
        $tableName = '';

        switch ($typ) {
            case "MC":
                $tableName = 'multiplechoices';
                break;
            case "DD":
                $tableName = 'draganddrops';
                break;
            case "TF":
                $tableName = 'trueandfalses';
                break;
            case "LT":
                $tableName = 'tffragens';
                break;
        }
        return $tableName;
    }

    /**
     * Converts Snippet type id into DB name.
     *
     * @param string 
     * @return string
     */
    public static function convertSnippetIdTypeToDbName($typ)
    {
        $tableName = '';

        switch ($typ) {
            case 1:
                $tableName = 'mcfragens';
                break;
            case 2:
                $tableName = 'trueandfalses';
                break;
            case 3:
                $tableName = 'draganddrops';
                break;
            case 4:
                $tableName = 'tffragens';
                break;
        }
        return $tableName;
    }

    /**
     * Converts Snippet type into name.
     *
     * @param string 
     * @return string
     */
    public static function convertSnippetTypeToName($typ)
    {
        $snippetName = '';

        switch ($typ) {
            case "MC":
                $snippetName = __('admin.MC');
                break;
            case "DD":
                $snippetName = __('admin.DD');
                break;
            case "TF":
                $snippetName = __('admin.TF');
                break;
            case "LT":
                $snippetName = __('admin.LT');
                break;
        }
        return $snippetName;
    }

}