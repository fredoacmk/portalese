<?php
/**
 * Class LPDataService | app/Services/LPDataService.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App\Services;

use App\SeiteToSnippet;
use App\Lebensbereich;
use App\Kompetenzstufe;

use Illuminate\Support\Facades\DB;

/**
* This class provides needed data for other php classes.
*/
class LPDataService
{

  /**
  * Provides Snippets from Seite id.
  *
  * @param int $id The id of the Seite.
  * @return array
  */
  public static function getSnippetsBySeiteId($id)
  {
    $temp[] = DB::table('mcfragens')
    ->where('idSeite', $id)
    ->get();
    $temp[0]['type'] = "MC";

    $temp[] = DB::table('trueandfalses')
    ->where('idSeite', $id)
    ->get();
    $temp[1]['type'] = "TF";

    if(count($temp[0]) === 1 && count($temp[1]) === 1) 
    {
        $temp = [];
    }

    return($temp);
  }

  // DEPRECATED !!!
    // /**
    //  * Provides Snippets from Seite id.
    //  *
    //  * @param int $id The id of the Seite.
    //  * @return string
    //  */
    // public function getSnippetsById($id)
    // {
    //   /* returns table SeiteToSnippet as array */
    //   $seiteToSnippet = SeiteToSnippet::select('seite_idSeite', 'snippet_idSnippet')
    //           ->where('seite_idSeite', $id)
    //           ->get()
    //           ->toArray();
  
    //   /* isolates SnippetID column from SeiteToSnippet */
    //   $isolatedSnippetID = array_column($seiteToSnippet, 'snippet_idSnippet');
  
    //   /* array for key/value-pairs */
    //   $list = [
    //       1 => "MC",
    //       2 => "TF",
    //       3 => "DD",
    //       4 => "LT"
    //   ];
  
    //   /* maps each key(snippetID) to according value */
    //   foreach($isolatedSnippetID as $key => $value){
    //     if($value == 1){
    //       $result[] = array($isolatedSnippetID[$key] => $list[1]);
    //     }
        
    //     if($value == 2){
    //       $result[] = array($isolatedSnippetID[$key] => $list[2]);
    //     }
    //     if($value == 3){
    //       $result[] = array($isolatedSnippetID[$key] => $list[3]);
    //     }
    //     if($value == 4){
    //       printf("not implemented yet");
    //     }
    //   }
  
    //   /* isolates type column from result array */
    //   $isolatedType = array();
    //   array_unshift($result, null);
    //   foreach (call_user_func_array("array_map", $result) as $value ) {
    //     $isolatedType[] = $value;
    //   }
  
    //       /* get called data as json */
    //       foreach($result as $iterate => $value){
    //           switch($iterate){
    //               case ($list[1]):
    //                   $temp[] = DB::table('mcfragens')
    //                   ->where('idMultipleChoice', $id)
    //                   ->get();
    //                   $temp[0]['type'] = "MC";
    //               case ($list[2]):
    //                   $temp[] = DB::table('trueandfalses')
    //                   ->join('tffragens', 'tffragens.idTrueFalse', '=', 'trueandfalses.idTrueFalse')
    //                   ->where('idSnippet', $id)
    //                   ->get();
    //                   $temp[1]['type'] = "TF";
    //               case ($list[3]):
    //                   //not done yet;
    //           }
    //       }
    //       return $temp;
    // }
}