<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MultipleChoice extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'multiplechoices';

    /**
     * The primary key column.
     *
     * @var string
     */
    protected $primaryKey = 'idMultipleChoice';

}
