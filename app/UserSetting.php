<?php
/**
 * Class UserSetting | app/UserSetting.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
* This class is the Laravel UserSetting model for database access. 
*/
class UserSetting extends Model
{
    //
}
