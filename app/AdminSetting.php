<?php
/**
 * Class AdminSetting | app/AdminSetting.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
* This class is the Laravel AdminSetting model for database access. 
*/
class AdminSetting extends Model
{
    //
}
