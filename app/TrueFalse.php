<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrueFalse extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'trueandfalses';

    /**
     * The primary key column.
     *
     * @var string
     */
    protected $primaryKey = 'id';
}
