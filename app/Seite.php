<?php
/**
 * Class Seite | app/Seite.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
* This class is the Laravel Seite model for database access. 
*/
class Seite extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'seiten';

    /**
     * The primary key column.
     *
     * @var string
     */
    protected $primaryKey = 'idSeite';
}
