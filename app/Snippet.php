<?php
/**
 * Class Kompetenzstufe | app/Kompetenzstufe.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
* This class is the Laravel Kompetenzstufe model for database access. 
*/
class Snippet extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'snippets';

    /**
     * The primary key column.
     *
     * @var string
     */
    protected $primaryKey = 'idSnippet';
}
