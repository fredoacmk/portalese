<?php
/**
 * Class Lebensbereich | app/Lebensbereich.php
 *
 * @package     App
 * @author      Team Leseportal
 * @version     v1.0.0
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
* This class is the Laravel Lebensbereich model for database access. 
*/
class Lebensbereich extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'lebensbereiche';

    /**
     * The primary key column.
     *
     * @var string
     */
    protected $primaryKey = 'idLebensbereich';
}
