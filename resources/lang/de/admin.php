<?php
/**
 * Created by PhpStorm.
 * User: dominique
 * Date: 28/11/2017
 * Time: 08:34
 */

return [
    'Adjustment' => 'Einstellungen',
    'Admin' => 'Admin',
    'AreaOfInterest' => 'Lebensbereiche',
    'Author' => 'Author',
    'Cancel' => 'Abbrechen',
    'CompetenceLevel' => 'Kompetenzstufen',
    'Create' => 'Erstellen',
    'CreatePage' => 'Seite erstellen',
    'CreateText' => 'Text erstellen',
    'Date' => 'Datum',
    'Description' => 'Beschreibung',
    'DragAndDrop' => 'Drag and Drop',
    'FileInput' => 'Datei hinzufügen',
    'InteractiveSlideShow' => 'Interagtive Slideshow',
    'InteractiveVideo' => 'Interagtives Video',
    'MediaLibrary' => 'Mediathek',
    'MultipleChoice' => 'Multiple Choice',
    'NumberOfSnippets' => 'Anzahl Snippets',
    'Page' => 'Seite',
    'Pages' => 'Seiten',
    'Save' => 'Speichern',
    'SchoolClass' => 'Klasse',
    'Search' => 'Suchen',
    'SkillLevel' => 'Kompetenzstufe',
    'SlpTemplate' => 'SLP-Vorlagen',
    'Snippets' => 'Snippets',
    'StatisticsOfLearning' => 'Lernstatistik',
    'Text' => 'Beitrag',
    'Title' => 'Titel',
    'TrueFalse' => 'True False',
    'Pages' => 'Seite',
    'User' => 'Benutzer',
    'Action' => 'Aktion',
    'Name' => 'Name',
    'Snippettype' => 'Snippet Typ',
    'MC' => 'Multiple Choice',
    'DD' => 'Drag and Drop',
    'TF' => 'Richtig / Falsch',
    'LT' => 'Lückentext',
    'SnippetId' => 'Snippet ID',
];