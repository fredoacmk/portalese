@extends('leseportal-layouts.home-master')

@section('content')

        <div class="flex-center position-ref full-height">
            <div class="content">
                <i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>
                <div class="title m-b-md">
                    Powered by Laravel
                </div>

                <div class="links">
                    <a href="http://leseportal.ch" target="_blank">Leseportal</a>
                    <a href="über-uns">Über uns</a>
                    <a href="kontakt">Kontakt</a>
                    <a href="admin">Admin</a>
                    <a href="lesen">User</a>
                </div>
            </div>
        </div>

@endsection
