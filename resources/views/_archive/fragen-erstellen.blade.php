@extends('_archive.layouts.master')

@php
$qcounter = 0;
$acounter = -1;
@endphp

@section('content')

<!DOCTYPE html>
<html>
<body>

    <div class="jumbotron">

        <form method="POST" action="/fragen">

            {{ csrf_field() }}

            <button type="submit" name="button" class="btn btn-success">Speichern</button>

            <div class="fs-titel">
                <label for="">Titel eingeben</label>
                <input class="form-control" name="title" type="text" placeholder="Title" value="<?=$text[0]->title;?>">
            </div>

            <div class="form-group">
                <label for="exampleFormControlTextarea1">Text eingeben</label>
                <textarea class="form-control" name="text" id="exampleFormControlTextarea1" rows="10"><?=$text[0]->text;?></textarea>
            </div>

            <div class="question">
                <h4>Fragen zum Text hinzufügen</h4>
            </div>


            @foreach ($q as $question)

            <div class="db-loaded">

                <div class="row">

                    <input class="form-control col-sm-10" name="question<?='_' . $qcounter;?>" type="text" placeholder="Title" value="<?=$question->frage;?>">

                    <i class="fa fa-trash-o col-sm-2 trasho" aria-hidden="true"></i>

                </div>


                @foreach ($answer as $a)

                @if ($a->frage_id == $question->id)

                <div class="col-4 gap">

                    <i class="fa fa-times" aria-hidden="true"></i>

                    <label name="label" class="checkbox">
                        <input class="form-control" name="answer<?='_' . $qcounter . '_' . ++$acounter; ?>" type="text" placeholder="Antwort" value="<?=$a->answer;?>">
                    </label>

                    <input type="checkbox" name="checkb<?='_' . $qcounter . '_' . $acounter; ?>" value="1"> Richtige Antwort

                </div>

                @endif

                @endforeach

                <div onclick="alert('add answer function - leider nocht nicht implementiert!')" class="add-q col-2">

                    <i class="fa fa-plus-circle" aria-hidden="true">Antwort hinzufügen</i>

                </div>

                @php
                ++$qcounter;
                $acounter = 0;
                @endphp

            </div>

            @endforeach


        </form>

        <div class="q-button">

            <button type="" name="button" onclick="alert('add question function - leider nocht nicht implementiert!')" class="btn btn-success">Frage hinzufügen</button>

        </div>
    </div>

    @endsection