@extends('_archive.layouts.master')

@section('content')

<!DOCTYPE html>
<html>
<body>

<div class="jumbotron">
    
    <div class="content-text">
        
        <div class="titel">
            <h4><?= $text[0]->title; ?></h4>
        </div>

        <div class="text">
            <p><?= $text[0]->text; ?></p>
        </div>

        <div class="titel">
            <h4>Fragen</h4>
        </div>

        <div class="question">

            @foreach ($q as $question)

            <div class="db-loaded">

                <p>{{ $question->frage }}</p>

                @foreach ($answer as $a)

                    @if ($a->frage_id == $question->id)
                        
                        <div class="form-check col-4">
                            
                            <input type="checkbox" class="form-check-input">{{ $a->answer }}</label>
                        
                        </div>

                    @endif

                @endforeach

                </div>

            @endforeach

        <button type="" name="button" onclick="alert('Alles richtig!')" class="btn btn-success">Auswerten</button>    


        </div>

    </div>

</div>

@endsection