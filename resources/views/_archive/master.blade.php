<!doctype html>
<html lang="en">

<head>

@include('_archive.layouts.head')

</head>


<body>

    @include('_archive.layouts.nav')

    @yield('content')

    @include('_archive.layouts.footer')

    <!-- jQuery -->
    <script src="js/jquery-3.2.1.js"></script>
    
    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script>



</body>
</html>

