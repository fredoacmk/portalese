<nav id="admin-sidebar">
    <!--div id="dismiss">
        <i class="fa fa-arrow-left" id="" aria-hidden="true"></i>
    </div-->

    <div class="sidebar-header">
        <h3>Leseportal Admin</h3>
    </div>

    <ul class="list-unstyled components">

        <li>
            <a href="/admin">
                <i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard
            </a>
        </li>

        <li>
            <a href="#seitenSubmenu" aria-hidden="true">
                <i class="fa fa-bar-chart" aria-hidden="true"></i> Beiträge
            </a>

            <ul class="list-unstyled components">
                <li>
                    <a href="/admin/seiten">
                        <i class="fa fa-list" aria-hidden="true"></i> Übersicht
                    </a>
                </li>
                <li>
                    <a href="/admin/seiten/erstellen">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i> Erstellen
                    </a>
                </li>
        <li>
            <a href="/admin/mediathek">
                <i class="fa fa-medium" aria-hidden="true"></i> Mediathek
            </a>
        </li>

        <li>
            <a href="#lebensbereicheSubmenu" data-toggle="collapse" aria-expanded="true">
                <i class="fa fa-leaf" aria-hidden="true"></i> Lebensbereiche <span class="caret pull-right"></span>
            </a>

            <ul class="collapse list-unstyled" id="lebensbereicheSubmenu">
                <li><a class="fa fa-list" href="/admin/lebensbereiche"> Übersicht</a></li>
                <li><a class="fa fa-plus-circle" href="/admin/lebensbereiche/erstellen"> Erstellen</a></li>
            </ul>
        </li>

        <li>
            <!--<a href="/admin/kompetenzstufe">-->
            <a href="#kompetenzstufeSubmenu" data-toggle="collapse" aria-expanded="true">
                <i class="fa fa-university" aria-hidden="true"></i> Kompetenzstufen <span class="caret pull-right"></span>
            </a>

            <ul class="collapse list-unstyled" id="kompetenzstufeSubmenu">
                <li><a class="fa fa-list" href="/admin/kompetenzstufe"> Übersicht</a></li>
                <li><a class="fa fa-plus-circle" href="/admin/kompetenzstufe/erstellen"> Erstellen</a></li>
            </ul>
        </li>
            </ul>
        </li>


        <li>
            <a href="#snippetsSubmenu" data-toggle="collapse" aria-expanded="true">
                <i class="fa fa-wrench" aria-hidden="true"></i> Snippets
            </a>

        </li>
        <ul class="list-unstyled components">
            <li>
                <a href="/admin/snippets">
                    <i class="fa fa-list" aria-hidden="true"></i> Übersicht
                </a>
            </li>
            <li>
                <a href="/admin/snippets/erstellen">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i> Erstellen
                </a>
            </li>
        </ul>

        <li>
            <a href="#snippetsSubmenu" data-toggle="collapse" aria-expanded="true">
                <i class="fa fa-graduation-cap" aria-hidden="true"></i> Meine Klasse
            </a>
            <ul class="list-unstyled components">
            <a href="/admin/lernstatistik">
                <i class="fa fa-bar-chart" aria-hidden="true"></i> Lernstatistik
            </a>
        </li>

        <li>
            <a href="/admin/benutzerverwaltung">
                <i class="fa fa-users" aria-hidden="true"></i> Benutzerverwaltung
            </a>
        </li>
            </ul>
    </ul>

</nav>