<nav id="user-sidebar">

    <div class="sidebar-header">
        <h3>Leseportal User</h3>
    </div>

    <ul class="list-unstyled components">

        <p>Hallo, {{ Auth::user()->name }}</p>

        <li>
            <a href="/lesen">
                <i class="fa fa-tachometer" aria-hidden="true"></i>Dashboard
            </a>
        </li>

        <li>
            <a href="#user-seitenSubmenu" data-toggle="collapse" aria-expanded="true">
                <i class="fa fa-file-text-o" aria-hidden="true"></i> Lebensbereiche <span class="caret pull-right"></span>
            </a>

            <ul class="collapse list-unstyled" id="user-seitenSubmenu"></ul>
        </li>

        <li>
            <a href="/lesen/lernstatistik">
                <i class="fa fa-bar-chart" aria-hidden="true"></i> Lernstatistik
            </a>
        </li>

        <li>
            <a href="/lesen/profil">
                <i class="fa fa-cogs" aria-hidden="true"></i> Einstellungen
            </a>
        </li>

    </ul>

</nav>

