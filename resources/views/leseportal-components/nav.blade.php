<header>

    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">

        <div class="col-md-3 col-sm-3 col-xs-5 nav-item menu">
            @auth
                <button type="button" id="sidebarCollapse" class="btn navbar-btn">
                    <i class="fa fa-bars" aria-hidden="true"> Menu</i>
                </button>
            @endauth
        </div>

        <div class="col-md-6 col-sm-6 col-xs-2 text-center nav-item logo">
            <img class="hidden-sm hidden-xs" src="{{URL::asset('media/logo-lp.png')}}" width="200px">
        </div>

        <!-- Authentication Links -->
        @if (Auth::guest())
            <a href="{{ route('login') }}" class="col-md-1 col-sm-1 col-xs-4 offset-md-10 offset-sm-10 offset-xs-4 nav-item nav-link">Anmelden</a>
            <a href="{{ route('register') }}" class="col-md-1 col-sm-1 col-xs-4 nav-item nav-link">Registrieren</a>
        @else
        <div class="col-md-3 col-sm-3 col-xs-5 nav-item dropdown">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                <i class="fa fa-user" aria-hidden="true"></i>
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <div class="dropdown-menu">
                @if (Auth::guard('web')->check())
                    <a href="/lesen/profil" class="dropdown-item">Mein Profil</a>

                    <a href="{{ route('logout') }}" class="dropdown-item"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>

                @elseif (Auth::guard('admin')->check())
                    <a href="/admin/profil" class="dropdown-item">Mein Profil</a>

                    <a href="{{ route('admin.logout') }}" class="dropdown-item"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                @endif
            </div>

        </div>
        @endif

        <!--/.nav-collapse -->
    </nav>

</header>

