@extends('leseportal-layouts.admin-master')


@section('content')

    @include('flash-message')

    <div class="container-fluid">

        <div class="leseportal-space">
            <div class="row">

                <div class="col-lg-2 col-mg-2 col-sm-2 col-xs-12">
                    <a href="{{ route('admin.lebensbereiche.erstellen') }}" class="btn btn-default">Erstellen</a>
                </div>
        </div>

        <div id="isLoading"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>

        <div class="leseportal-space" id="body-container">
            <div class="row">
                <div class="col-lg-12 col-mg-12 col-sm-12 col-xs-12">
                    <table class="table table-bordered" id="overViewTable">

                        <thead>
                        <tr>
                            <th>{{__('admin.Name')}}</th>
                            <th>{{__('admin.Description')}}</th>
                            <th>{{__('admin.Action')}}</th>
                        </tr>
                        </thead>

                        <tbody></tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
        @section('page-script')
            <script>
                populateAreaOfInterest();
            </script>
@endsection