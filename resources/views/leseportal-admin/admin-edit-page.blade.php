@extends('leseportal-layouts.admin-master')


@section('content')
    <form method="post" action="{{ action('AdminSeitenController@update', $seitenModified[0]['idSeite']) }}">
    {{ csrf_field() }}

    <!-- insert title of the text -->
        <div class="leseportal-space">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                    <p>{{__('admin.Title')}}</p>
                </div>
                <div class="col-lg-10 col-md-10 col-sm-9 col-xs-12">
                    <input type="text" class="form-control" id="usr" name="title" value="{{ $seitenModified[0]['titel']}}" required>
                </div>
            </div>

        </div>

        <!-- insert text -->
        <div class="leseportal-space">

            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                    <p>{{__('admin.Text')}}</p>
                </div>

                <div class="col-lg-10 col-md-10 col-sm-9 col-xs-12" id="editor-container">
                    <div id="isLoading"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                    <textarea id="article-ckeditor" style="display: none;" name="content" value="" required >{{ $seitenModified[0]['inhalt']}}</textarea>
                </div>
            </div>

        </div>

        <!-- add area of interest for text -->
        <div class="leseportal-space">

            <div class="row">

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                    <p>{{__('admin.AreaOfInterest')}}</p>
                </div>

                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">

                    <select id="interest" class="form-control" name="interest" required>

                        <option value="{{ $seitenModified[0]['idLebensbereich']}}" >{{ $seitenModified[0]['lebensbereich']}}    </option>

                    </select>

                </div>

            </div>

        </div>

        <!-- add skill level for text -->
        <div class="leseportal-space">

            <div class="row">

                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                    <p>{{__('admin.SkillLevel')}}</p>
                </div>

                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">

                    <select id="competence" class="form-control" name="competence" required>

                        <option value="{{ $seitenModified[0]['idKompetenzstufe']}}" >{{ $seitenModified[0]['kompetenzstufe']}}</option>

                    </select>

                </div>

            </div>
        </div>

        <!-- button for save new text and button for delete changes and go back -->
        <div class="leseportal-space">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-lg-offset-2 col-md-offset-2 col-sm-offset-3">
                    <button id="cbtn" type="button" class="btn btn-danger">{{__('admin.Cancel')}}</button>
                    <button type="submit" class="btn btn-success">{{__('admin.Save')}}</button>
                </div>
            </div>
        </div>

    </form>

@endsection

@section('page-script')
    <script>
        $("#isLoading").show();

        CKEDITOR.replace( 'article-ckeditor', {
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
        });
        
        populateInterest();
        populateCompetence();
        registerCancelButtonEditArticle("#cbtn");

        $("#isLoading").hide();
        $("#editor-container").show();
    </script>

@endsection