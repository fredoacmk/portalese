@extends('leseportal-layouts.admin-master')

@section('content')

    <div class="row tile_count" id="dashB">

        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-file-text-o" aria-hidden="true"></i> Total Seiten</span>
            <div id="seiten" class="count"></div>
        </div>

        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-leaf icon" aria-hidden="true"></i> Total Lebensbereiche</span>
            <div id="lb" class="count green"></div>
        </div>

        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-wrench" aria-hidden="true"></i> Total Snippets</span>
            <div id="snippets" class="count"></div>
        </div>

        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> Total Benutzer</span>
            <div id="users" class="count"></div>
        </div>

        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user-o" aria-hidden="true"></i> Total Admins</span>
            <div id="admins" class="count"></div>
        </div>

    </div>

@endsection


@section('page-script')
    <script>
        populateDashboardAdmin();
    </script>
@endsection