@extends('leseportal-layouts.admin-master')

@section('content')

    <form method="post" action="{{ action('AdminLebensbereichController@update', $lebensbereich['idLebensbereich'])}}">
    {{ csrf_field() }}

    <!-- insert title of the text -->
        <div class="leseportal-space">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                    <p>{{__('admin.Title')}}</p>
                </div>
                <div class="col-lg-10 col-md-10 col-sm-9 col-xs-12">
                    <input type="text" class="form-control" id="usr" name="title" value="{{$lebensbereich['name']}}" required>
                </div>
            </div>

        </div>

        <!-- insert text -->
        <div class="leseportal-space">

            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                    <p>{{__('admin.Description')}}</p>
                </div>

                <div class="col-lg-10 col-md-10 col-sm-9 col-xs-12">
                    <textarea class="form-control" name="content" rows="3" required >{{$lebensbereich['beschreibung']}}</textarea>
                </div>
            </div>

        </div>

        <!-- button for save new text and button for delete changes and go back -->
        <div class="leseportal-space">

            <div class="row">

                <div class="col-lg-6 col-md-6 col-sm-6 col-lg-offset-2 col-md-offset-2 col-sm-offset-3">
                    <button id="cbtn" type="button" class="btn btn-danger">{{__('admin.Cancel')}}</button>
                    <button type="submit" class="btn btn-success">{{__('admin.Save')}}</button>
                </div>
            </div>
        </div>

    </form>

@endsection

@section('page-script')
    <script>
        registerCancelButtonEditAreaOfInterest("#cbtn");
    </script>

@endsection