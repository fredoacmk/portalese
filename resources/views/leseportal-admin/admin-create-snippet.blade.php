@extends('leseportal-layouts.admin-master')


@section('content')

    <form method="post" action="{{ action('AdminSnippetController@store') }}">
    {{ csrf_field() }}

    <!-- search bar -->
        <div class="leseportal-space">
            <div class="row">
                <div class="col-lg-4 col-mg-4 col-sm-4 col-xs-8">
                    <input type="text" class="form-control" id="usr">
                </div>
                <div class="col-lg-8 col-mg-8 col-sm-8 col-xs-2">
                    <button type="button" class="btn btn-default">{{__('admin.Search')}}</button>
                </div>
            </div>
        </div>

        <!-- list of all texts on leseportal -->
        <div class="leseportal-space">
            <div class="row">
                <div class="col-lg-7 col-mg-7 col-sm-7 col-xs-12">

                    <!-- list group -->
                    <div class="list-group" id="snippetTextList" role="tablist"></div>
                    <input id="pages_input_hidden" name="idSeite" type="hidden" value="">
                </div>
            </div>
        </div>

        <!-- add kind of snippet -->
        <div class="leseportal-space">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                    <p>{{__('admin.Snippettype')}}</p>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                    <select id="snippettype" class="form-control" name="snippettype" required></select>
                </div>
            </div>
        </div>

        <!-- create snippet section -->
        <div id="create-snippet-section">

        </div>

        <!-- button for save new text and button for delete changes and go back -->
        <div class="leseportal-space snippet-save-block">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <button id="cbtn" type="button" class="btn btn-danger">{{__('admin.Cancel')}}</button>
                    <button type="submit" class="btn btn-success">{{__('admin.Save')}}</button>
                </div>
            </div>
        </div>

    </form>


@endsection

@section('page-script')
    <script>
        populatePagesCreateSnippet();
        populateSnippettype();
        showSnippetSection();
        registerCancelButtonSnippet("#cbtn");
    </script>
@endsection


