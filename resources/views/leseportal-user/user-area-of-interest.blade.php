@extends('leseportal-layouts.user-master')


@section('content')

    <div class="inner" id="inner-area-of-interest">
        <h2 class="title"></h2>
        <div class="row">
            <div class="card-group">
                <div class="card">

                    @foreach($seitenModified as $seite)

                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="thumbnail">
                                <div class="card-block">
                                </div>
                            </div>
                        </div>

                    @endforeach

                </div>
            </div>
        </div>
    </div>


@endsection

@section('page-script')
    <script>
        setTitleInterest();
        loadAreaOfInterest();
    </script>
@endsection

