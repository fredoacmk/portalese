@extends('leseportal-layouts.user-master')


@section('content')

    <div class="inner-user">
        <h1>Auswertung</h1>
        {{--<p>{{ dd($combined) }}</p>--}}
        <div class="leseportal-space control-group" id="show-results-section"></div>
    </div>

@endsection

@section('page-script')
    <script>
        showResults(@json($combined));
    </script>
@endsection

