@extends('leseportal-layouts.user-master')


@section('content')

<!-- heading for page user dashboard -->
<!-- 5 example boxes which will be filled with content later -->

    <div class="inner" id="inner-dashboard">

        <div class="row">

            <div class="card-group">

                <div class="card">

                    @foreach($seitenModified as $seite)

                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 cols">
                        <div class="thumbnail">
                            {{--<img src="https://static.pexels.com/photos/669615/pexels-photo-669615.jpeg">--}}
                            <div class="card-block">
                            </div>
                        </div>
                    </div>

                    @endforeach
                    
                </div>
            </div>
        </div>
    </div>


@endsection

@section('page-script')
    <script>
        loadDashboard();
    </script>
@endsection

