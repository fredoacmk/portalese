@extends('leseportal-layouts.user-master')


@section('content')

    <div class="inner-user">
        {!! $seite->inhalt !!}


        <hr>

        <form method="post" action="{{ action('ResultController@evaluate') }}">
            {{ csrf_field() }}
            
            <input id="page-show_input_hidden" name="idSeite" type="hidden" value={{$seite->idSeite}}>

            <div class="leseportal-space control-group" id="show-snippets-section">
            {{--  <h2 class="title">Snippettype MC</h2>
                <h3>Frage 1</h3>

                <div class="row">
                    <div class="col-lg-5 col-md-7 col-sm-8 col-xs-12">
                        <h4>Frage</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-5 col-md-7 col-sm-8 col-xs-12">
                        <label class="control control--checkbox">placeholder for some answer
                            <input type="checkbox" name="" value="" checked=""/>
                            <div class="control__indicator"></div>
                        </label>
                    </div>
                </div>--}}
            </div>

            <!-- button for save new text and button for delete changes and go back -->
            <div class="leseportal-space submit-block">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <button type="submit" class="btn btn-success">{{__('user.Evaluate')}}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('page-script')
    <script>
        showSnippets({{$seite->idSeite}});
    </script>
@endsection