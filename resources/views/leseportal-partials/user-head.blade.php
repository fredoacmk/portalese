
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="leseportal" content="">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script type="text/javascript" src="{{ URL::asset('/js/lib/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/js/lib/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/js/lib/setup.min.js') }}"></script>
    
    <link href="{{ URL::asset('/css/lib/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('/css/lib/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('/css/leseportal-user.css') }}" rel="stylesheet" type="text/css">
    
    <script src="{{ URL::asset('https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <link href="{{ URL::asset('https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css') }}" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="{{ URL::asset('/js/leseportal-user.js') }}"></script>