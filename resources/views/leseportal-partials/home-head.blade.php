
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="leseportal" content="">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script type="text/javascript" src="{{ URL::asset('/js/lib/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/js/lib/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/js/lib/setup.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/js/leseportal-home.js') }}"></script>

    <link href="{{ URL::asset('/css/lib/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('/css/lib/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('/css/leseportal-home.css') }}" rel="stylesheet" type="text/css">
