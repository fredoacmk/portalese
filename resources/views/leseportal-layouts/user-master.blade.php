<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>

    @include('leseportal-partials.user-head')

</head>


<body>


    <div class="wrapper">

        @include('leseportal-components.user-sidemenu')
        @include('leseportal-components.nav')
        
        <div id="content">
            @yield('content')
        </div>
    </div>
    
    <div class="overlay"></div>
    
    @include('leseportal-components.userstat')
    @yield('page-script')

</body>
</html>

