<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>

@include('leseportal-partials.home-head')

</head>


<body>

    @include('leseportal-components.nav')

    @yield('content')

    @include('leseportal-components.footer')

    @yield('page-script')

</body>
</html>

