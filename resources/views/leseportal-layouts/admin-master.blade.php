<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>

    @include('leseportal-partials.admin-head')

</head>


<body>



<div class="wrapper">

    @include('leseportal-components.admin-sidemenu')
    @include('leseportal-components.nav')

    <div id="content">
        @yield('content')
    </div>
</div>

<div class="overlay"></div>

@yield('page-script')

</body>
</html>

