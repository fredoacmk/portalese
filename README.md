# Leseportal

Improve reading and hearing skills.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to dev:

```
XAMPP (or similar software) to localhost the server (Apache/Nginx & MySQL).
php >= 7.0.0 (might come with XAMPP)
mysql (comes with XAMPP)
git

composer
PhpStorm
``` 

## What to do?

### MAKE SURE COMPOSER AND PHP ARE INSTALLED ON YOUR MACHINE. OTHERWISE YOU CAN'T CONTINUE HERE!

The following commands have to be executed ind cmd/terminal **inside the project folder**! So navigate to project folder.

1. First pull from repository (by clone or whatever)
1. **git checkout develop**
1. Copy-Paste (duplicate) file `.env.example` and rename one to **.env**
1. Copy-Paste (duplicate) files `composer.json.copy_rename` and `composer.lock.copy_rename` and rename them to: **composer.json** and **composer.lock**
1. **composer install** in command line (very important!). This downloads all the dependencies defined in `composer.lock` and `composer.json`. Otherwise you can not work with the framework, because dependencies are missing (/vendor folder defined in .gitignore).
1. **php artisan key:generate** in command line. This will create an application key which is needed to run the application.
1. **composer dump-autoload**
1. If using XAMPP, point XAMPP documentroot to `project_folder/public`. Change settings in `XAMPP/apache/conf/httpd.conf`
1. Check if you can see the web site on localhost.


After all dependencies have been downloaded (after composer install), YOU DON'T HAVE TO DO IT AGAIN unless you have series issues, a new clone or new dependency.

## Troubelshooting

If there is "Whoops somethin..." or another _error_ text, make following sure:

1. PHP and/or composer is installed correctly.
1. PHP extensions for mbstring and openSSL is enabled in php_folder/php.ini
1. .env is renamed correctly.
1. Check read/write access folder where project is.
1. Check php artisan key is generated.
1. Read error message and ask google.

## How to proceed

After you successfully get a standard laravel page served (it does not yet have any styles), you can continue with the how-tos in "./tools/docs/".

To get the full page running, you need to at least follow the HowToMigrateSeed tutorial. For the whole develop environment you need to follow all tutorials.


## Some best practices

1. After completing your editing/DEV'ing, always have a look at the changed files with `git status` and re-check if there are only the files YOU changed by purpose. If there are changed files you did not touch, check the changes and make sure these changes don't influence others environment.
1. With above said, avoid commands that change multiple files at once (e.g. composer update).
